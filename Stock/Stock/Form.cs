﻿using Stock.code;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Stock {
    public partial class Form :System.Windows.Forms.Form {
        private string exchangePass = "###";
        private int ownedStock = 0;
        private int balance = 0;
        private float currentStockMinPrize = 0.01f;
        private float currentStockMaxPrize = float.MaxValue;

        private List<Parameter> parameters;
        public Form() {
            InitializeComponent();
            user.Text = "ndt0001";
            password.Text = "123";
        }

        private void Form_Load(object sender, EventArgs e) {
            parameters = new List<Parameter>();
            hideAllPanel();
            mainPanel.Visible = true;
        }

        private void hideAllPanel() {
            mainPanel.Visible = false;
            exchangePanel.Visible = false;
            stockUpdatePanel.Visible = false;
            saokePanel.Visible = false;
            userInfoPanel.Visible = false;
        }

        private void setExchangePanel() {
            hideAllPanel();
            exchangePanel.Visible = true;
            sellCheck.Checked = true;
            loCheck.Checked = true;
            stockPrize.Enabled = loCheck.Checked;
            //stockInfo.Visible = false;
            loadBankData(bankID);
            loadStockData();
        }

        private void loadBankData(ComboBox cb) {
            //load banks to combobox
            String sql = "exec SP_GET_TK_SOHUU '" + Program.userId + "'";
            DataTable dt = Program.ExecSqlQuery(sql);
            cb.DataSource = dt;
            cb.ValueMember = "MATK";
            cb.DisplayMember = "TENNH";
        }

        private void loadStockData() {
            //load stocks to combobox
            String sql = "";
            parameters.Clear();
            if(sellCheck.Checked) {
                parameters.Add(new Parameter("@MANDT", SqlDbType.NChar, Program.userId));
                sql = "dbo.SP_GET_ALL_SOHUU";
            } else {
                sql = "dbo.SP_GET_ALL_COPHIEU";
            }
            DataTable dataTable = Program.ExecDataTable(sql, parameters);
            stockID.DataSource = dataTable;
            stockID.ValueMember = "MACP";
            stockID.DisplayMember = "MACP";
        }

        private void showWarning(Label lb) {
            lb.Visible = true;
            BackgroundWorker back = new BackgroundWorker();
            back.WorkerReportsProgress = true;
            //do in background
            back.DoWork += new DoWorkEventHandler(delegate (object o, DoWorkEventArgs args) {
                BackgroundWorker bw = o as BackgroundWorker;
                System.Threading.Thread.Sleep(1000);
            });
            //do when progress change
            back.ProgressChanged += new ProgressChangedEventHandler(delegate (object o, ProgressChangedEventArgs args) {
                //set progress here with {args.ProgressPercentage}
            });
            //do when complete task
            back.RunWorkerCompleted += new RunWorkerCompletedEventHandler(delegate (object o, RunWorkerCompletedEventArgs args) {
                lb.Visible = false;
            });
            back.RunWorkerAsync();
        }

        private void resetSelection() {
            btnThongke.BackColor = Color.Transparent;
            btnExchange.BackColor = Color.Transparent;
            btnSetting.BackColor = Color.Transparent;
            btnUpdate.BackColor = Color.Transparent;
            btnSaoke.BackColor = Color.Transparent;
        }

        private void btnExchange_Click(object sender, EventArgs e) {
            if(!exchangePanel.Visible) {
                resetSelection();
                btnExchange.BackColor = Color.DarkGreen;
                setExchangePanel();
            }
        }

        private void btnSetting_Click(object sender, EventArgs e) {
            resetSelection();
            btnThongke.BackColor = Color.DarkGreen;
        }

        private void setMenuPanel() {
            loginPanel.Visible = false;
            if(Program.userGroup.Trim().Equals("NDT")) {
                txtUserID.Text = Program.userId;
                txtUserName.Text = Program.userName;
                menuPanel.Visible = true;
            }else {
                Debug.WriteLine(Program.userGroup);
                txtUserID.Text = Program.userId;
                txtUserName.Text = Program.userName;
                adminPanel.Visible = true;
            }
        }

        private void btnLogin_Click(object sender, EventArgs e) {
            if(Program.connect(user.Text.Trim(), password.Text) == 0) {
                showWarning(info);
            } else {
                String sql = "exec SP_DANGNHAP_ALL '" + user.Text.Trim() + "'";
                try {
                    Program.myReader = Program.ExecSqlDataReader(sql);
                } catch(InvalidOperationException ex) {
                    showWarning(info);
                }
                if(Program.myReader.Read()) {
                    Program.userId = Program.myReader.GetString(0);
                    Program.userName = Program.myReader.GetString(1);
                    Program.userGroup = Program.myReader.GetString(2);
                    setMenuPanel();
                } else {
                    showWarning(info);
                }
                Program.myReader.Close();
                Program.conn.Close();
            }
        }
        
        private void setSaoKePanel() {
            hideAllPanel();
            saokePanel.Visible = true;
            setSaoKeTable("dbo.SP_SAO_KE");
        }

        private void setSaoKeTable(String sql) {
            parameters.Clear();
            parameters.Add(new Parameter("@MANDT", SqlDbType.NChar, Program.userId));
            parameters.Add(new Parameter("@NGAYFROM", SqlDbType.DateTime, dateFrom.Value));
            parameters.Add(new Parameter("@NGAYTO", SqlDbType.DateTime, dateTo.Value));
            DataTable dt = Program.ExecDataTable(sql, parameters);
            saokeDataContainer.DataSource = dt;
        }

        private void sellCheck_CheckedChanged(object sender, EventArgs e) {
            setPrize();
            loadStockData();
            calculatingPrize();
        }

        private void buyCheck_CheckedChanged(object sender, EventArgs e) {
            setPrize();
            loadStockData();
            calculatingPrize();
        }

        private void stockID_SelectedIndexChanged(object sender, EventArgs e) {
            if(buyCheck.Checked) {

            }
            if(sellCheck.Checked) {

            }
            setCurrentStockInfo();
            setPrize();
            calculatingPrize();
        }

        private void setCurrentStockInfo() {
            txtStockID.Text = stockID.SelectedValue.ToString();
            txtGiaSan.Text = "###";
            txtGiaTran.Text = "###";
            txtGiaTC.Text = "###";
            String sql = "exec SP_GET_TT_CP '" + stockID.SelectedValue.ToString() + "'";
            DataTable dt = Program.ExecSqlQuery(sql);
            foreach(DataRow row in dt.Rows) {
                txtGiaSan.Text = row["GIASAN"].ToString();
                txtGiaTran.Text = row["GIATRAN"].ToString();
                txtGiaTC.Text = row["GIATC"].ToString();
                currentStockMinPrize = float.Parse(row["GIASAN"].ToString());
                currentStockMaxPrize = float.Parse(row["GIATRAN"].ToString());
            }
            parameters.Clear();
            sql = "dbo.SP_GET_SOHUU";
            parameters.Add(new Parameter("@MANDT", SqlDbType.NChar, Program.userId));
            parameters.Add(new Parameter("@MACP", SqlDbType.NChar, stockID.SelectedValue.ToString()));
            try {
                if(Program.ExecIntOutPut(sql, parameters, "@OWN") != DBNull.Value) {
                    ownedStock = int.Parse(Program.ExecIntOutPut(sql, parameters, "@OWN").ToString());
                } else {
                    ownedStock = 0;
                }
            } catch(Exception ex) {
                MessageBox.Show(ex.ToString());
            }
            txtOwnStockAmount.Text = ownedStock.ToString();
        }

        private void loCheck_CheckedChanged(object sender, EventArgs e) {
            setPrize();
            stockPrize.Enabled = true;
            calculatingPrize();
        }

        private void setPrize() {
            if(currentStockMaxPrize == float.MaxValue)
                return;
            if(buyCheck.Checked) {
                if(mpCheck.Checked)
                    stockPrize.Value = (Decimal) currentStockMaxPrize;
                else
                    stockPrize.Value = (Decimal) currentStockMinPrize;
            } else {
                if(mpCheck.Checked)
                    stockPrize.Value = (Decimal) currentStockMinPrize;
                else
                    stockPrize.Value = (Decimal) currentStockMaxPrize;
            }
        }

        private void mpCheck_CheckedChanged(object sender, EventArgs e) {
            setPrize();
            stockPrize.Enabled = false;
            calculatingPrize();
        }

        private void bankID_SelectedIndexChanged(object sender, EventArgs e) {
            txtBankID.Text = bankID.SelectedValue.ToString();
            DataTable dataTable = new DataTable();
            SqlCommand command = new SqlCommand("dbo.TT_NDT", Program.conn);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add("@MANDT", SqlDbType.NChar).Value = Program.userId;
            command.Parameters.Add("@MATK", SqlDbType.NChar).Value = bankID.SelectedValue.ToString();
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(dataTable);
            foreach(DataRow row in dataTable.Rows) {
                txtBalance.Text = row["SOTIEN"].ToString();
                exchangePass = row["MKGD"].ToString();
                txtBankName.Text = row["TENNH"].ToString();
            }

            parameters.Clear();
            String sql = "dbo.SP_GET_SO_DU_KHADUNG";
            parameters.Add(new Parameter("@MANDT", SqlDbType.NChar, Program.userId));
            parameters.Add(new Parameter("@MATK", SqlDbType.NChar, bankID.SelectedValue.ToString()));
            try {
                if(Program.ExecIntOutPut(sql, parameters, "@OWN") != DBNull.Value) {
                    balance = int.Parse(Program.ExecIntOutPut(sql, parameters, "@OWN").ToString());
                } else {
                    balance = 0;
                }
            } catch(Exception ex) {
                MessageBox.Show(ex.ToString());
            }
            txtAvailableBalance.Text = balance.ToString();
            calculatingPrize();
        }

        private void exchangePassword_TextChanged(object sender, EventArgs e) {
            if(txtExchangePassWarning.Visible)
                txtExchangePassWarning.Visible = false;
        }

        private void setUserInfoPanel() {
            hideAllPanel();
            userInfoPanel.Visible = true;
            loadBankData(cbInfoPanelBankID);
            String sql = "exec dbo.SP_GET_SOHUU_DETAIL '"+Program.userId+"'";
            DataTable dt = Program.ExecSqlQuery(sql);
            infoPanelDataGrid.DataSource = dt;
        }

        private void btnSetting_Click_1(object sender, EventArgs e) {
            if(!userInfoPanel.Visible) {
                resetSelection();
                btnSetting.BackColor = Color.DarkGreen;
                setUserInfoPanel();
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e) {
            resetSelection();
            btnUpdate.BackColor = Color.DarkGreen;
            hideAllPanel();
            stockUpdatePanel.Visible = true;
            fillUpdateData();
        }

        private void fillUpdateData() {
            if(listCP.Items.Count > 0)
                listCP.Items.Clear();
            DataTable dt = Program.ExecSqlQuery("exec dbo.SP_SAN_GD");
            sanGDGrid.DataSource = dt;
            foreach(DataRow r in dt.Rows) {
                Debug.WriteLine("aaa");
                ListViewItem lvi = new ListViewItem(r["MACP"].ToString());
                lvi.SubItems.Add(r["TENCTY"].ToString());
                lvi.SubItems.Add(r["DIACHI"].ToString());
                lvi.SubItems.Add(r["GIA"].ToString());
                lvi.UseItemStyleForSubItems = false;
                var gia = float.Parse(r["GIA"].ToString());
                var giatc = float.Parse(r["GIATC"].ToString());
                lvi.SubItems.Add((Math.Round(gia - giatc,2)).ToString());
                if(gia - giatc > 0) {
                    lvi.SubItems[4].BackColor = Color.DarkGreen;
                }else if(gia - giatc < 0) {
                    lvi.SubItems[4].BackColor = Color.DarkRed;
                }else {
                    lvi.SubItems[4].BackColor = Color.RoyalBlue;
                }
                lvi.SubItems[4].ForeColor = Color.White;
                listCP.Items.Add(lvi);
            }
        }

        private void btnSaoke_Click(object sender, EventArgs e) {
            if(!saokePanel.Visible) {
                resetSelection();
                btnSaoke.BackColor = Color.DarkGreen;
                setSaoKePanel();
                loadLenhCho();
            }
        }

        private void loadLenhCho() {
            if(listLenhCho.Items.Count > 0)
                listLenhCho.Items.Clear();
            parameters.Clear();
            parameters.Add(new Parameter("@MANDT", SqlDbType.NChar, Program.userId));
            DataTable dataTable = Program.ExecDataTable("dbo.SP_GET_LENH_CHO", parameters);
            foreach(DataRow row in dataTable.Rows) {
                ListViewItem lvi = new ListViewItem(row["MAGD"].ToString());
                lvi.SubItems.Add(row["MACP"].ToString());
                lvi.SubItems.Add(row["LOAILENH"].ToString());
                lvi.SubItems.Add(row["SOLUONG"].ToString());
                lvi.SubItems.Add(row["GIA"].ToString());
                lvi.SubItems.Add(row["TRANGTHAI"].ToString());
                listLenhCho.Items.Add(lvi);
            }
        }

        private void btnCommit_Click(object sender, EventArgs e) {
            if(!exchangePassword.Text.Equals(exchangePass)) {
                txtExchangePassWarning.Visible = true;
                return;
            }
            if((int) stockAmount.Value == 0) {
                txtAmountWarning.Text = "Số lượng không thể bằng 0!";
                txtAmountWarning.Visible = true;
                return;
            }
            if((int) stockPrize.Value <= 0 && loCheck.Checked) {
                txtPrizeWarning.Text = "Giá không thể bằng 0!";
                txtPrizeWarning.Visible = true;
                return;
            }
            if(buyCheck.Checked) {
                if(((float) stockPrize.Value < currentStockMinPrize || (float) stockPrize.Value >= currentStockMaxPrize) && loCheck.Checked) {
                    txtPrizeWarning.Text = "Giá phải nằm trong khoảng giá sàn và giá trần!";
                    txtPrizeWarning.Visible = true;
                    return;
                }
                if(mpCheck.Checked && balance < currentStockMaxPrize * (int) stockAmount.Value) {
                    txtAmountWarning.Text = "Số dư hiện tại không đủ!";
                    txtAmountWarning.Visible = true;
                    return;
                }
                if(balance < (float) stockPrize.Value * (int) stockAmount.Value) {
                    txtAmountWarning.Text = "Số dư hiện tại không đủ!";
                    txtAmountWarning.Visible = true;
                    return;
                }
                parameters.Clear();
                parameters.Add(new Parameter("@LOAILENH", SqlDbType.Char, "M"));
            }
            if(sellCheck.Checked) {
                if((int) stockAmount.Value <= 0 || (int) stockAmount.Value > ownedStock) {
                    txtAmountWarning.Text = "Số lượng sở hữu không đủ đáp ứng!";
                    txtAmountWarning.Visible = true;
                    return;
                }
                parameters.Clear();
                parameters.Add(new Parameter("@LOAILENH", SqlDbType.Char, "B"));
            }
            try {
                String pt = loCheck.Checked ? "LO" : "MP";
                parameters.Add(new Parameter("@PHUONGTHUC", SqlDbType.NChar, pt));
                parameters.Add(new Parameter("@SOLUONG", SqlDbType.Int, (int) stockAmount.Value));
                parameters.Add(new Parameter("@MACP", SqlDbType.NChar, stockID.SelectedValue.ToString()));
                parameters.Add(new Parameter("@GIA", SqlDbType.Float, Math.Round((float) stockPrize.Value, 2)));
                parameters.Add(new Parameter("@MATK", SqlDbType.NChar, bankID.SelectedValue.ToString()));
                parameters.Add(new Parameter("@MANDT", SqlDbType.NChar, Program.userId));
                String sql = "dbo.SP_EXCHANGE";
                Program.ExecDataTable(sql, parameters);
                MessageBox.Show(sellCheck.Checked ? "Đặt bán" : "Đặt mua" + " thành công!\nCổ phiếu: " + stockID.SelectedValue
                    + "\nSố lượng: " + stockAmount.Value.ToString()
                    + "\nGiá: " + Math.Round((float) stockPrize.Value, 2).ToString()
                    + "\nPhương thức giao dịch: " + pt);

                setCurrentStockInfo();
                loadBankData(bankID);
                ownedStock = 0;
                stockAmount.Value = 1;
                stockPrize.Value = 0;
                exchangePassword.Text = "";
            } catch(Exception ex) {
                Debug.WriteLine(ex.ToString());
            }
        }

        private void btnAddLogin_Click(object sender, EventArgs e) {
            if(!addLoginPanel.Visible) {
                resetSelection();
                btnAddLogin.BackColor = Color.DarkGreen;
                setAddLoginPanel();
            }
        }

        private void setAddLoginPanel() {
            adminCheck.Checked = true;
            hideAllPanel();
            addLoginPanel.Visible = true;
            txtCreateUserID.Text = Program.ExecStringOutPut("dbo.GETMANV", "@OUTPUT").ToString();
        }

        private void btnBackupAndRestore_Click(object sender, EventArgs e) {

        }

        private void btnLogoutAdmin_Click(object sender, EventArgs e) {
            logout();
        }

        private void btnSaoKeInsize_Click(object sender, EventArgs e) {
            btnSaoKeKhopInsize.BackColor = Color.RoyalBlue;
            btnSaoKeInsize.BackColor = Color.DarkGreen;
            setSaoKeTable("dbo.SP_SAO_KE");
        }

        private void btnSaoKeKhopInsize_Click(object sender, EventArgs e) {
            btnSaoKeInsize.BackColor = Color.RoyalBlue;
            btnSaoKeKhopInsize.BackColor = Color.DarkGreen;
            setSaoKeTable("dbo.SP_SAO_KE_KHOP_HET");
        }

        private void btnInfoPanelChangeUserPass_Click(object sender, EventArgs e) {
            var dialog = new Login(this, null);
            dialog.Show();
        }

        private void txtInfoPanelBankName_Click(object sender, EventArgs e) {

        }

        private void cbInfoPanelBankID_SelectedIndexChanged(object sender, EventArgs e) {
            String sql = "dbo.TT_NDT";
            parameters.Clear();
            parameters.Add(new Parameter("@MANDT", SqlDbType.NChar, Program.userId));
            parameters.Add(new Parameter("@MATK", SqlDbType.NChar, cbInfoPanelBankID.SelectedValue.ToString()));
            DataTable dt = Program.ExecDataTable(sql, parameters);
            foreach(DataRow row in dt.Rows) {
                txtInfoPanelUserID.Text = row["MANDT"].ToString();
                txtInfoPanelUserName.Text = row["HOTEN"].ToString();
                txtInfoPanelBankIDInfo.Text = cbInfoPanelBankID.SelectedValue.ToString();
                txtInfoPanelBankName.Text = row["TENNH"].ToString();
                txtInfoPanelBalance.Text = row["SOTIEN"].ToString();
            }
        }

        private void button1_Click(object sender, EventArgs e) {
            var form = new Login(this, exchangePass);
            form.Show();
        }

        private void btnLogout_Click(object sender, EventArgs e) {
            logout();
        }

        private void logout() {
            resetSelection();
            hideAllPanel();
            adminPanel.Visible = false;
            menuPanel.Visible = false;
            mainPanel.Visible = true;
            loginPanel.Visible = true;
            info.Visible = false;
            user.Text = "";
            password.Text = "";
            Program.logout();
        }

        private void adminCheck_CheckedChanged(object sender, EventArgs e) {
            txtCreateUserID.Text = Program.ExecStringOutPut("dbo.GETMANV", "@OUTPUT").ToString();
        }

        private void ndtCheck_CheckedChanged(object sender, EventArgs e) {
            txtCreateUserID.Text = Program.ExecStringOutPut("dbo.GETMANDT", "@OUTPUT").ToString();
        }

        private void btnCreateLogin_Click(object sender, EventArgs e) {

        }

        private void stockAmount_KeyUp(object sender, KeyEventArgs e) {
            if(txtAmountWarning.Visible)
                txtAmountWarning.Visible = false;
            calculatingPrize();
        }

        private void stockPrize_KeyUp(object sender, KeyEventArgs e) {
            if(txtPrizeWarning.Visible)
                txtPrizeWarning.Visible = false;
            calculatingPrize();
        }

        private void calculatingPrize() {
            var prize = mpCheck.Checked && buyCheck.Checked ? currentStockMaxPrize : (float) stockPrize.Value;
            var sum = prize * (int) stockAmount.Value;
            if(sum > balance && buyCheck.Checked) {
                txtSumMoney.Text = sum.ToString();
                if(txtSumMoney.BackColor != Color.DarkRed)
                    txtSumMoney.BackColor = Color.DarkRed;
            } else {
                txtSumMoney.Text = sum.ToString();
                if(txtSumMoney.BackColor != Color.DarkGreen)
                    txtSumMoney.BackColor = Color.DarkGreen;
            }
        }

        private void listLenhCho_SelectedIndexChanged(object sender, EventArgs e) {
            if(!btnHuyLenh.Enabled)
                btnHuyLenh.Enabled = true;
            if(listLenhCho.SelectedItems.Count<=0 && btnHuyLenh.Enabled) {
                btnHuyLenh.Enabled = false;
            }
        }

        private void btnHuyLenh_Click(object sender, EventArgs e) {
            parameters.Clear();
            parameters.Add(new Parameter("@MAGD", SqlDbType.NChar, listLenhCho.Items[listLenhCho.SelectedIndices[0]].SubItems[0].Text.ToString()));
            Program.ExecDataTable("dbo.SP_HUY_LENH", parameters);
            loadLenhCho();
            btnHuyLenh.Enabled = false;
        }

        private void btnUngTien_Click(object sender, EventArgs e) {
            new FormLoan().Show();
        }
    }
}
