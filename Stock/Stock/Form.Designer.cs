﻿namespace Stock {
    partial class Form {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form));
            this.menuPanel = new System.Windows.Forms.Panel();
            this.btnSetting = new System.Windows.Forms.Button();
            this.btnThongke = new System.Windows.Forms.Button();
            this.btnSaoke = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnExchange = new System.Windows.Forms.Button();
            this.btnLogout = new System.Windows.Forms.Button();
            this.txtUserName = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtUserID = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.loginPanel = new System.Windows.Forms.Panel();
            this.info = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.btnLogin = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.password = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.user = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.mainPanel = new System.Windows.Forms.Panel();
            this.exchangePanel = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.mpCheck = new System.Windows.Forms.RadioButton();
            this.label9 = new System.Windows.Forms.Label();
            this.loCheck = new System.Windows.Forms.RadioButton();
            this.btnCommit = new System.Windows.Forms.Button();
            this.panel9 = new System.Windows.Forms.Panel();
            this.txtExchangePassWarning = new System.Windows.Forms.Label();
            this.bankInfo = new System.Windows.Forms.Panel();
            this.txtAvailableBalance = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtBalance = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtBankName = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.txtBankID = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.exchangePassword = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.bankID = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.sumMoneyPanel = new System.Windows.Forms.Panel();
            this.txtSumMoney = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.txtPrizeWarning = new System.Windows.Forms.Label();
            this.txtAmountWarning = new System.Windows.Forms.Label();
            this.stockAmount = new System.Windows.Forms.NumericUpDown();
            this.stockPrize = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.stockInfo = new System.Windows.Forms.Panel();
            this.txtGiaTC = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtGiaSan = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtGiaTran = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtOwnStockAmount = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtStockID = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.stockID = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.radioPanel = new System.Windows.Forms.Panel();
            this.buyCheck = new System.Windows.Forms.RadioButton();
            this.sellCheck = new System.Windows.Forms.RadioButton();
            this.adminPanel = new System.Windows.Forms.Panel();
            this.btnAddNH = new System.Windows.Forms.Button();
            this.btnAddCP = new System.Windows.Forms.Button();
            this.btnBackupAndRestore = new System.Windows.Forms.Button();
            this.btnAddLogin = new System.Windows.Forms.Button();
            this.btnLogoutAdmin = new System.Windows.Forms.Button();
            this.txtAdminName = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.txtAdminID = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.saokePanel = new System.Windows.Forms.Panel();
            this.btnHuyLenh = new System.Windows.Forms.Button();
            this.listLenhCho = new System.Windows.Forms.ListView();
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label42 = new System.Windows.Forms.Label();
            this.panel20 = new System.Windows.Forms.Panel();
            this.saokeDataContainer = new System.Windows.Forms.DataGridView();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnSaoKeKhopInsize = new System.Windows.Forms.Button();
            this.btnSaoKeInsize = new System.Windows.Forms.Button();
            this.dateTo = new System.Windows.Forms.DateTimePicker();
            this.label46 = new System.Windows.Forms.Label();
            this.dateFrom = new System.Windows.Forms.DateTimePicker();
            this.label45 = new System.Windows.Forms.Label();
            this.stockUpdatePanel = new System.Windows.Forms.Panel();
            this.listCP = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.sanGDGrid = new System.Windows.Forms.DataGridView();
            this.userInfoPanel = new System.Windows.Forms.Panel();
            this.infoPanelDataGrid = new System.Windows.Forms.DataGridView();
            this.label26 = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.txtInfoPanelBalance = new System.Windows.Forms.Label();
            this.txtInfoPanelBankName = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.txtInfoPanelBankIDInfo = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.cbInfoPanelBankID = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.txtInfoPanelUserExchagePass = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.btnInfoPanelChangeUserPass = new System.Windows.Forms.Button();
            this.txtInfoPanelUserPass = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.txtInfoPanelUserName = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtInfoPanelUserID = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.addLoginPanel = new System.Windows.Forms.Panel();
            this.btnDeleteLogin = new System.Windows.Forms.Button();
            this.txtLoginInfoDelete = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.panel17 = new System.Windows.Forms.Panel();
            this.btnCreateLogin = new System.Windows.Forms.Button();
            this.dtUserBirth = new System.Windows.Forms.DateTimePicker();
            this.label35 = new System.Windows.Forms.Label();
            this.tbCreateUserEmail = new System.Windows.Forms.TextBox();
            this.tbCreateCMND = new System.Windows.Forms.TextBox();
            this.tbCreateUserPhone = new System.Windows.Forms.TextBox();
            this.tbCreateUserAddr = new System.Windows.Forms.TextBox();
            this.tbCreateUserPass = new System.Windows.Forms.TextBox();
            this.tbCreateUserName = new System.Windows.Forms.TextBox();
            this.txtCreateUserID = new System.Windows.Forms.Label();
            this.panel16 = new System.Windows.Forms.Panel();
            this.ndtCheck = new System.Windows.Forms.RadioButton();
            this.adminCheck = new System.Windows.Forms.RadioButton();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.addCPPanel = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.cbCreateOwnUser = new System.Windows.Forms.ComboBox();
            this.tbCreateCPPrize = new System.Windows.Forms.NumericUpDown();
            this.tbCreateCPAmount = new System.Windows.Forms.NumericUpDown();
            this.tbCreateCPAddr = new System.Windows.Forms.TextBox();
            this.tbCreateCPComName = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnUngTien = new System.Windows.Forms.Button();
            this.menuPanel.SuspendLayout();
            this.loginPanel.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.exchangePanel.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel9.SuspendLayout();
            this.bankInfo.SuspendLayout();
            this.panel8.SuspendLayout();
            this.sumMoneyPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stockAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stockPrize)).BeginInit();
            this.panel1.SuspendLayout();
            this.stockInfo.SuspendLayout();
            this.radioPanel.SuspendLayout();
            this.adminPanel.SuspendLayout();
            this.saokePanel.SuspendLayout();
            this.panel20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.saokeDataContainer)).BeginInit();
            this.panel4.SuspendLayout();
            this.stockUpdatePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sanGDGrid)).BeginInit();
            this.userInfoPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.infoPanelDataGrid)).BeginInit();
            this.panel13.SuspendLayout();
            this.panel14.SuspendLayout();
            this.addLoginPanel.SuspendLayout();
            this.panel16.SuspendLayout();
            this.addCPPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbCreateCPPrize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCreateCPAmount)).BeginInit();
            this.SuspendLayout();
            // 
            // menuPanel
            // 
            this.menuPanel.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.menuPanel.Controls.Add(this.btnSetting);
            this.menuPanel.Controls.Add(this.btnThongke);
            this.menuPanel.Controls.Add(this.btnSaoke);
            this.menuPanel.Controls.Add(this.btnUpdate);
            this.menuPanel.Controls.Add(this.btnExchange);
            this.menuPanel.Controls.Add(this.btnLogout);
            this.menuPanel.Controls.Add(this.txtUserName);
            this.menuPanel.Controls.Add(this.panel3);
            this.menuPanel.Controls.Add(this.txtUserID);
            this.menuPanel.Controls.Add(this.panel2);
            this.menuPanel.Location = new System.Drawing.Point(0, 0);
            this.menuPanel.Name = "menuPanel";
            this.menuPanel.Size = new System.Drawing.Size(240, 600);
            this.menuPanel.TabIndex = 0;
            this.menuPanel.Visible = false;
            // 
            // btnSetting
            // 
            this.btnSetting.BackColor = System.Drawing.Color.Transparent;
            this.btnSetting.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnSetting.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DarkGreen;
            this.btnSetting.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkGreen;
            this.btnSetting.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSetting.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSetting.ForeColor = System.Drawing.Color.White;
            this.btnSetting.Image = ((System.Drawing.Image)(resources.GetObject("btnSetting.Image")));
            this.btnSetting.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSetting.Location = new System.Drawing.Point(-2, 324);
            this.btnSetting.Name = "btnSetting";
            this.btnSetting.Size = new System.Drawing.Size(246, 36);
            this.btnSetting.TabIndex = 9;
            this.btnSetting.Text = "Thông tin tài khoản";
            this.btnSetting.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSetting.UseVisualStyleBackColor = false;
            this.btnSetting.Click += new System.EventHandler(this.btnSetting_Click_1);
            // 
            // btnThongke
            // 
            this.btnThongke.BackColor = System.Drawing.Color.Transparent;
            this.btnThongke.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnThongke.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DarkGreen;
            this.btnThongke.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkGreen;
            this.btnThongke.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnThongke.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThongke.ForeColor = System.Drawing.Color.White;
            this.btnThongke.Image = ((System.Drawing.Image)(resources.GetObject("btnThongke.Image")));
            this.btnThongke.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnThongke.Location = new System.Drawing.Point(-2, 324);
            this.btnThongke.Name = "btnThongke";
            this.btnThongke.Size = new System.Drawing.Size(246, 36);
            this.btnThongke.TabIndex = 8;
            this.btnThongke.Text = "Thống kê";
            this.btnThongke.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnThongke.UseVisualStyleBackColor = false;
            this.btnThongke.Visible = false;
            // 
            // btnSaoke
            // 
            this.btnSaoke.BackColor = System.Drawing.Color.Transparent;
            this.btnSaoke.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnSaoke.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DarkGreen;
            this.btnSaoke.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkGreen;
            this.btnSaoke.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSaoke.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaoke.ForeColor = System.Drawing.Color.White;
            this.btnSaoke.Image = ((System.Drawing.Image)(resources.GetObject("btnSaoke.Image")));
            this.btnSaoke.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSaoke.Location = new System.Drawing.Point(-3, 282);
            this.btnSaoke.Name = "btnSaoke";
            this.btnSaoke.Size = new System.Drawing.Size(246, 36);
            this.btnSaoke.TabIndex = 7;
            this.btnSaoke.Text = "Sao kê";
            this.btnSaoke.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSaoke.UseVisualStyleBackColor = false;
            this.btnSaoke.Click += new System.EventHandler(this.btnSaoke_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackColor = System.Drawing.Color.Transparent;
            this.btnUpdate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnUpdate.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DarkGreen;
            this.btnUpdate.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkGreen;
            this.btnUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.ForeColor = System.Drawing.Color.White;
            this.btnUpdate.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdate.Image")));
            this.btnUpdate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUpdate.Location = new System.Drawing.Point(0, 242);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(246, 36);
            this.btnUpdate.TabIndex = 6;
            this.btnUpdate.Text = "Bảng giá điện tử";
            this.btnUpdate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnExchange
            // 
            this.btnExchange.BackColor = System.Drawing.Color.Transparent;
            this.btnExchange.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnExchange.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DarkGreen;
            this.btnExchange.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkGreen;
            this.btnExchange.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExchange.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExchange.ForeColor = System.Drawing.Color.White;
            this.btnExchange.Image = ((System.Drawing.Image)(resources.GetObject("btnExchange.Image")));
            this.btnExchange.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExchange.Location = new System.Drawing.Point(-2, 200);
            this.btnExchange.Name = "btnExchange";
            this.btnExchange.Size = new System.Drawing.Size(246, 36);
            this.btnExchange.TabIndex = 5;
            this.btnExchange.Text = "Mua bán cổ phiếu";
            this.btnExchange.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExchange.UseVisualStyleBackColor = false;
            this.btnExchange.Click += new System.EventHandler(this.btnExchange_Click);
            // 
            // btnLogout
            // 
            this.btnLogout.BackColor = System.Drawing.Color.Transparent;
            this.btnLogout.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnLogout.FlatAppearance.BorderSize = 0;
            this.btnLogout.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DarkRed;
            this.btnLogout.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkRed;
            this.btnLogout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogout.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogout.ForeColor = System.Drawing.Color.White;
            this.btnLogout.Image = ((System.Drawing.Image)(resources.GetObject("btnLogout.Image")));
            this.btnLogout.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLogout.Location = new System.Drawing.Point(12, 519);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(120, 36);
            this.btnLogout.TabIndex = 4;
            this.btnLogout.Text = "Đăng xuất";
            this.btnLogout.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLogout.UseVisualStyleBackColor = false;
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // txtUserName
            // 
            this.txtUserName.AutoSize = true;
            this.txtUserName.BackColor = System.Drawing.Color.Transparent;
            this.txtUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUserName.ForeColor = System.Drawing.Color.White;
            this.txtUserName.Location = new System.Drawing.Point(5, 490);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(104, 20);
            this.txtUserName.TabIndex = 3;
            this.txtUserName.Text = "NDT0000001";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Location = new System.Drawing.Point(0, 450);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(240, 2);
            this.panel3.TabIndex = 2;
            // 
            // txtUserID
            // 
            this.txtUserID.AutoSize = true;
            this.txtUserID.BackColor = System.Drawing.Color.Transparent;
            this.txtUserID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUserID.ForeColor = System.Drawing.Color.White;
            this.txtUserID.Location = new System.Drawing.Point(5, 460);
            this.txtUserID.Name = "txtUserID";
            this.txtUserID.Size = new System.Drawing.Size(104, 20);
            this.txtUserID.TabIndex = 1;
            this.txtUserID.Text = "NDT0000001";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel2.BackgroundImage")));
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panel2.Location = new System.Drawing.Point(70, 50);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(100, 100);
            this.panel2.TabIndex = 0;
            // 
            // loginPanel
            // 
            this.loginPanel.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.loginPanel.Controls.Add(this.info);
            this.loginPanel.Controls.Add(this.panel7);
            this.loginPanel.Controls.Add(this.panel6);
            this.loginPanel.Controls.Add(this.panel5);
            this.loginPanel.Location = new System.Drawing.Point(0, 0);
            this.loginPanel.Name = "loginPanel";
            this.loginPanel.Size = new System.Drawing.Size(240, 600);
            this.loginPanel.TabIndex = 10;
            // 
            // info
            // 
            this.info.AutoSize = true;
            this.info.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.info.ForeColor = System.Drawing.Color.Maroon;
            this.info.Location = new System.Drawing.Point(48, 350);
            this.info.Name = "info";
            this.info.Size = new System.Drawing.Size(144, 16);
            this.info.TabIndex = 3;
            this.info.Text = "Invalid ID or Password!";
            this.info.Visible = false;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.btnLogin);
            this.panel7.Location = new System.Drawing.Point(0, 283);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(240, 50);
            this.panel7.TabIndex = 2;
            // 
            // btnLogin
            // 
            this.btnLogin.BackColor = System.Drawing.Color.Transparent;
            this.btnLogin.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkGreen;
            this.btnLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogin.ForeColor = System.Drawing.Color.White;
            this.btnLogin.Image = ((System.Drawing.Image)(resources.GetObject("btnLogin.Image")));
            this.btnLogin.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLogin.Location = new System.Drawing.Point(60, 8);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(120, 32);
            this.btnLogin.TabIndex = 0;
            this.btnLogin.Text = "Login";
            this.btnLogin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLogin.UseVisualStyleBackColor = false;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.password);
            this.panel6.Controls.Add(this.label2);
            this.panel6.Location = new System.Drawing.Point(0, 223);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(240, 50);
            this.panel6.TabIndex = 2;
            // 
            // password
            // 
            this.password.BackColor = System.Drawing.Color.White;
            this.password.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.password.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.password.Location = new System.Drawing.Point(60, 15);
            this.password.Name = "password";
            this.password.Size = new System.Drawing.Size(163, 22);
            this.password.TabIndex = 1;
            this.password.UseSystemPasswordChar = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(5, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "Pass";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.user);
            this.panel5.Controls.Add(this.label1);
            this.panel5.Location = new System.Drawing.Point(0, 163);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(240, 50);
            this.panel5.TabIndex = 0;
            // 
            // user
            // 
            this.user.BackColor = System.Drawing.Color.White;
            this.user.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.user.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.user.Location = new System.Drawing.Point(60, 15);
            this.user.Name = "user";
            this.user.Size = new System.Drawing.Size(163, 22);
            this.user.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(5, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "User";
            // 
            // mainPanel
            // 
            this.mainPanel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("mainPanel.BackgroundImage")));
            this.mainPanel.Location = new System.Drawing.Point(240, 0);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(1025, 565);
            this.mainPanel.TabIndex = 10;
            this.mainPanel.Visible = false;
            // 
            // exchangePanel
            // 
            this.exchangePanel.Controls.Add(this.panel10);
            this.exchangePanel.Controls.Add(this.btnCommit);
            this.exchangePanel.Controls.Add(this.panel9);
            this.exchangePanel.Controls.Add(this.panel8);
            this.exchangePanel.Controls.Add(this.panel1);
            this.exchangePanel.Controls.Add(this.radioPanel);
            this.exchangePanel.Location = new System.Drawing.Point(240, 0);
            this.exchangePanel.Name = "exchangePanel";
            this.exchangePanel.Size = new System.Drawing.Size(1025, 565);
            this.exchangePanel.TabIndex = 11;
            // 
            // panel10
            // 
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel10.Controls.Add(this.mpCheck);
            this.panel10.Controls.Add(this.label9);
            this.panel10.Controls.Add(this.loCheck);
            this.panel10.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.panel10.Location = new System.Drawing.Point(31, 190);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(960, 50);
            this.panel10.TabIndex = 4;
            // 
            // mpCheck
            // 
            this.mpCheck.AutoSize = true;
            this.mpCheck.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mpCheck.Location = new System.Drawing.Point(300, 10);
            this.mpCheck.Name = "mpCheck";
            this.mpCheck.Size = new System.Drawing.Size(46, 20);
            this.mpCheck.TabIndex = 3;
            this.mpCheck.TabStop = true;
            this.mpCheck.Text = "MP";
            this.mpCheck.UseVisualStyleBackColor = true;
            this.mpCheck.CheckedChanged += new System.EventHandler(this.mpCheck_CheckedChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(60, 13);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(103, 16);
            this.label9.TabIndex = 0;
            this.label9.Text = "Phương thức đặt";
            // 
            // loCheck
            // 
            this.loCheck.AutoSize = true;
            this.loCheck.FlatAppearance.BorderColor = System.Drawing.SystemColors.MenuHighlight;
            this.loCheck.FlatAppearance.BorderSize = 2;
            this.loCheck.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loCheck.Location = new System.Drawing.Point(200, 10);
            this.loCheck.Name = "loCheck";
            this.loCheck.Size = new System.Drawing.Size(43, 20);
            this.loCheck.TabIndex = 2;
            this.loCheck.TabStop = true;
            this.loCheck.Text = "LO";
            this.loCheck.UseVisualStyleBackColor = true;
            this.loCheck.CheckedChanged += new System.EventHandler(this.loCheck_CheckedChanged);
            // 
            // btnCommit
            // 
            this.btnCommit.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnCommit.FlatAppearance.BorderSize = 0;
            this.btnCommit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DarkGreen;
            this.btnCommit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkGreen;
            this.btnCommit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCommit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCommit.ForeColor = System.Drawing.Color.White;
            this.btnCommit.Image = ((System.Drawing.Image)(resources.GetObject("btnCommit.Image")));
            this.btnCommit.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCommit.Location = new System.Drawing.Point(30, 500);
            this.btnCommit.Name = "btnCommit";
            this.btnCommit.Size = new System.Drawing.Size(200, 36);
            this.btnCommit.TabIndex = 5;
            this.btnCommit.Text = "Đặt Lệnh";
            this.btnCommit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCommit.UseVisualStyleBackColor = false;
            this.btnCommit.Click += new System.EventHandler(this.btnCommit_Click);
            // 
            // panel9
            // 
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel9.Controls.Add(this.txtExchangePassWarning);
            this.panel9.Controls.Add(this.bankInfo);
            this.panel9.Controls.Add(this.exchangePassword);
            this.panel9.Controls.Add(this.label8);
            this.panel9.Controls.Add(this.bankID);
            this.panel9.Controls.Add(this.label6);
            this.panel9.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.panel9.Location = new System.Drawing.Point(30, 360);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(960, 100);
            this.panel9.TabIndex = 4;
            // 
            // txtExchangePassWarning
            // 
            this.txtExchangePassWarning.AutoSize = true;
            this.txtExchangePassWarning.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtExchangePassWarning.ForeColor = System.Drawing.Color.DarkRed;
            this.txtExchangePassWarning.Location = new System.Drawing.Point(380, 64);
            this.txtExchangePassWarning.Name = "txtExchangePassWarning";
            this.txtExchangePassWarning.Size = new System.Drawing.Size(85, 16);
            this.txtExchangePassWarning.TabIndex = 8;
            this.txtExchangePassWarning.Text = "Sai mật khẩu";
            this.txtExchangePassWarning.Visible = false;
            // 
            // bankInfo
            // 
            this.bankInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bankInfo.Controls.Add(this.txtAvailableBalance);
            this.bankInfo.Controls.Add(this.label13);
            this.bankInfo.Controls.Add(this.txtBalance);
            this.bankInfo.Controls.Add(this.label12);
            this.bankInfo.Controls.Add(this.txtBankName);
            this.bankInfo.Controls.Add(this.label21);
            this.bankInfo.Controls.Add(this.txtBankID);
            this.bankInfo.Controls.Add(this.label23);
            this.bankInfo.Location = new System.Drawing.Point(480, -1);
            this.bankInfo.Name = "bankInfo";
            this.bankInfo.Size = new System.Drawing.Size(480, 100);
            this.bankInfo.TabIndex = 10;
            // 
            // txtAvailableBalance
            // 
            this.txtAvailableBalance.AutoSize = true;
            this.txtAvailableBalance.BackColor = System.Drawing.Color.DarkGreen;
            this.txtAvailableBalance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAvailableBalance.ForeColor = System.Drawing.Color.White;
            this.txtAvailableBalance.Location = new System.Drawing.Point(280, 70);
            this.txtAvailableBalance.Name = "txtAvailableBalance";
            this.txtAvailableBalance.Size = new System.Drawing.Size(43, 16);
            this.txtAvailableBalance.TabIndex = 7;
            this.txtAvailableBalance.Text = "#####";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(150, 70);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(104, 16);
            this.label13.TabIndex = 6;
            this.label13.Text = "Số dư khả dụng:";
            // 
            // txtBalance
            // 
            this.txtBalance.AutoSize = true;
            this.txtBalance.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.txtBalance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBalance.ForeColor = System.Drawing.Color.White;
            this.txtBalance.Location = new System.Drawing.Point(80, 70);
            this.txtBalance.Name = "txtBalance";
            this.txtBalance.Size = new System.Drawing.Size(43, 16);
            this.txtBalance.TabIndex = 5;
            this.txtBalance.Text = "#####";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(15, 70);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(46, 16);
            this.label12.TabIndex = 4;
            this.label12.Text = "Số dư:";
            // 
            // txtBankName
            // 
            this.txtBankName.AutoSize = true;
            this.txtBankName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBankName.Location = new System.Drawing.Point(150, 40);
            this.txtBankName.Name = "txtBankName";
            this.txtBankName.Size = new System.Drawing.Size(43, 16);
            this.txtBankName.TabIndex = 3;
            this.txtBankName.Text = "#####";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(15, 40);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(77, 16);
            this.label21.TabIndex = 2;
            this.label21.Text = "Ngân hàng:";
            // 
            // txtBankID
            // 
            this.txtBankID.AutoSize = true;
            this.txtBankID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBankID.Location = new System.Drawing.Point(150, 10);
            this.txtBankID.Name = "txtBankID";
            this.txtBankID.Size = new System.Drawing.Size(50, 16);
            this.txtBankID.TabIndex = 1;
            this.txtBankID.Text = "abcxyz";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(15, 10);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(123, 16);
            this.label23.TabIndex = 0;
            this.label23.Text = "Thông tin tài khoản:";
            // 
            // exchangePassword
            // 
            this.exchangePassword.Location = new System.Drawing.Point(200, 64);
            this.exchangePassword.Name = "exchangePassword";
            this.exchangePassword.Size = new System.Drawing.Size(160, 20);
            this.exchangePassword.TabIndex = 6;
            this.exchangePassword.UseSystemPasswordChar = true;
            this.exchangePassword.TextChanged += new System.EventHandler(this.exchangePassword_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(60, 64);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(120, 16);
            this.label8.TabIndex = 2;
            this.label8.Text = "Mật khẩu giao dịch";
            // 
            // bankID
            // 
            this.bankID.FormattingEnabled = true;
            this.bankID.Location = new System.Drawing.Point(200, 18);
            this.bankID.Name = "bankID";
            this.bankID.Size = new System.Drawing.Size(160, 21);
            this.bankID.TabIndex = 1;
            this.bankID.SelectedIndexChanged += new System.EventHandler(this.bankID_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(60, 19);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 16);
            this.label6.TabIndex = 0;
            this.label6.Text = "Tài khoản";
            // 
            // panel8
            // 
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.sumMoneyPanel);
            this.panel8.Controls.Add(this.txtPrizeWarning);
            this.panel8.Controls.Add(this.txtAmountWarning);
            this.panel8.Controls.Add(this.stockAmount);
            this.panel8.Controls.Add(this.stockPrize);
            this.panel8.Controls.Add(this.label7);
            this.panel8.Controls.Add(this.label5);
            this.panel8.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.panel8.Location = new System.Drawing.Point(30, 250);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(960, 100);
            this.panel8.TabIndex = 3;
            // 
            // sumMoneyPanel
            // 
            this.sumMoneyPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.sumMoneyPanel.Controls.Add(this.txtSumMoney);
            this.sumMoneyPanel.Controls.Add(this.label43);
            this.sumMoneyPanel.Location = new System.Drawing.Point(600, -1);
            this.sumMoneyPanel.Name = "sumMoneyPanel";
            this.sumMoneyPanel.Size = new System.Drawing.Size(360, 102);
            this.sumMoneyPanel.TabIndex = 12;
            // 
            // txtSumMoney
            // 
            this.txtSumMoney.AutoSize = true;
            this.txtSumMoney.BackColor = System.Drawing.Color.DarkGreen;
            this.txtSumMoney.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSumMoney.ForeColor = System.Drawing.Color.White;
            this.txtSumMoney.Location = new System.Drawing.Point(155, 40);
            this.txtSumMoney.Name = "txtSumMoney";
            this.txtSumMoney.Size = new System.Drawing.Size(43, 16);
            this.txtSumMoney.TabIndex = 11;
            this.txtSumMoney.Text = "#####";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(21, 40);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(124, 16);
            this.label43.TabIndex = 10;
            this.label43.Text = "Tổng giá trị dự kiến:";
            // 
            // txtPrizeWarning
            // 
            this.txtPrizeWarning.AutoSize = true;
            this.txtPrizeWarning.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrizeWarning.ForeColor = System.Drawing.Color.DarkRed;
            this.txtPrizeWarning.Location = new System.Drawing.Point(400, 16);
            this.txtPrizeWarning.Name = "txtPrizeWarning";
            this.txtPrizeWarning.Size = new System.Drawing.Size(120, 16);
            this.txtPrizeWarning.TabIndex = 7;
            this.txtPrizeWarning.Text = "Giá không phù hợp";
            this.txtPrizeWarning.Visible = false;
            // 
            // txtAmountWarning
            // 
            this.txtAmountWarning.AutoSize = true;
            this.txtAmountWarning.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAmountWarning.ForeColor = System.Drawing.Color.DarkRed;
            this.txtAmountWarning.Location = new System.Drawing.Point(400, 61);
            this.txtAmountWarning.Name = "txtAmountWarning";
            this.txtAmountWarning.Size = new System.Drawing.Size(152, 16);
            this.txtAmountWarning.TabIndex = 6;
            this.txtAmountWarning.Text = "Số lượng không phù hợp";
            this.txtAmountWarning.Visible = false;
            // 
            // stockAmount
            // 
            this.stockAmount.Location = new System.Drawing.Point(200, 61);
            this.stockAmount.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.stockAmount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.stockAmount.Name = "stockAmount";
            this.stockAmount.Size = new System.Drawing.Size(160, 20);
            this.stockAmount.TabIndex = 5;
            this.stockAmount.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.stockAmount.KeyUp += new System.Windows.Forms.KeyEventHandler(this.stockAmount_KeyUp);
            // 
            // stockPrize
            // 
            this.stockPrize.DecimalPlaces = 2;
            this.stockPrize.Location = new System.Drawing.Point(200, 16);
            this.stockPrize.Maximum = new decimal(new int[] {
            -727379968,
            232,
            0,
            0});
            this.stockPrize.Name = "stockPrize";
            this.stockPrize.Size = new System.Drawing.Size(160, 20);
            this.stockPrize.TabIndex = 4;
            this.stockPrize.KeyUp += new System.Windows.Forms.KeyEventHandler(this.stockPrize_KeyUp);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(60, 21);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 16);
            this.label7.TabIndex = 3;
            this.label7.Text = "Giá";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(60, 61);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 16);
            this.label5.TabIndex = 2;
            this.label5.Text = "Số lượng";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.stockInfo);
            this.panel1.Controls.Add(this.stockID);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.panel1.Location = new System.Drawing.Point(30, 80);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(960, 100);
            this.panel1.TabIndex = 2;
            // 
            // stockInfo
            // 
            this.stockInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.stockInfo.Controls.Add(this.txtGiaTC);
            this.stockInfo.Controls.Add(this.label16);
            this.stockInfo.Controls.Add(this.txtGiaSan);
            this.stockInfo.Controls.Add(this.label14);
            this.stockInfo.Controls.Add(this.txtGiaTran);
            this.stockInfo.Controls.Add(this.label11);
            this.stockInfo.Controls.Add(this.txtOwnStockAmount);
            this.stockInfo.Controls.Add(this.label10);
            this.stockInfo.Controls.Add(this.txtStockID);
            this.stockInfo.Controls.Add(this.label4);
            this.stockInfo.Location = new System.Drawing.Point(480, -1);
            this.stockInfo.Name = "stockInfo";
            this.stockInfo.Size = new System.Drawing.Size(480, 100);
            this.stockInfo.TabIndex = 2;
            // 
            // txtGiaTC
            // 
            this.txtGiaTC.AutoSize = true;
            this.txtGiaTC.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.txtGiaTC.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGiaTC.ForeColor = System.Drawing.Color.White;
            this.txtGiaTC.Location = new System.Drawing.Point(370, 70);
            this.txtGiaTC.Name = "txtGiaTC";
            this.txtGiaTC.Size = new System.Drawing.Size(29, 16);
            this.txtGiaTC.TabIndex = 9;
            this.txtGiaTC.Text = "###";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(300, 70);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 16);
            this.label16.TabIndex = 8;
            this.label16.Text = "Giá TC:";
            // 
            // txtGiaSan
            // 
            this.txtGiaSan.AutoSize = true;
            this.txtGiaSan.BackColor = System.Drawing.Color.DarkRed;
            this.txtGiaSan.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGiaSan.ForeColor = System.Drawing.Color.White;
            this.txtGiaSan.Location = new System.Drawing.Point(220, 70);
            this.txtGiaSan.Name = "txtGiaSan";
            this.txtGiaSan.Size = new System.Drawing.Size(29, 16);
            this.txtGiaSan.TabIndex = 7;
            this.txtGiaSan.Text = "###";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(150, 70);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(57, 16);
            this.label14.TabIndex = 6;
            this.label14.Text = "Giá sàn:";
            // 
            // txtGiaTran
            // 
            this.txtGiaTran.AutoSize = true;
            this.txtGiaTran.BackColor = System.Drawing.Color.DarkGreen;
            this.txtGiaTran.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGiaTran.ForeColor = System.Drawing.Color.White;
            this.txtGiaTran.Location = new System.Drawing.Point(80, 70);
            this.txtGiaTran.Name = "txtGiaTran";
            this.txtGiaTran.Size = new System.Drawing.Size(29, 16);
            this.txtGiaTran.TabIndex = 5;
            this.txtGiaTran.Text = "###";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(15, 70);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(57, 16);
            this.label11.TabIndex = 4;
            this.label11.Text = "Giá trần:";
            // 
            // txtOwnStockAmount
            // 
            this.txtOwnStockAmount.AutoSize = true;
            this.txtOwnStockAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOwnStockAmount.Location = new System.Drawing.Point(150, 40);
            this.txtOwnStockAmount.Name = "txtOwnStockAmount";
            this.txtOwnStockAmount.Size = new System.Drawing.Size(43, 16);
            this.txtOwnStockAmount.TabIndex = 3;
            this.txtOwnStockAmount.Text = "#####";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(15, 40);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(85, 16);
            this.label10.TabIndex = 2;
            this.label10.Text = "Đang sở hữu:";
            // 
            // txtStockID
            // 
            this.txtStockID.AutoSize = true;
            this.txtStockID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStockID.Location = new System.Drawing.Point(150, 10);
            this.txtStockID.Name = "txtStockID";
            this.txtStockID.Size = new System.Drawing.Size(50, 16);
            this.txtStockID.TabIndex = 1;
            this.txtStockID.Text = "abcxyz";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(15, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(128, 16);
            this.label4.TabIndex = 0;
            this.label4.Text = "Thông Tin Cổ phiếu:";
            // 
            // stockID
            // 
            this.stockID.FormattingEnabled = true;
            this.stockID.Location = new System.Drawing.Point(200, 41);
            this.stockID.Name = "stockID";
            this.stockID.Size = new System.Drawing.Size(160, 21);
            this.stockID.TabIndex = 1;
            this.stockID.SelectedIndexChanged += new System.EventHandler(this.stockID_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(60, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 16);
            this.label3.TabIndex = 0;
            this.label3.Text = "Cổ phiếu";
            // 
            // radioPanel
            // 
            this.radioPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.radioPanel.Controls.Add(this.buyCheck);
            this.radioPanel.Controls.Add(this.sellCheck);
            this.radioPanel.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.radioPanel.Location = new System.Drawing.Point(30, 20);
            this.radioPanel.Name = "radioPanel";
            this.radioPanel.Size = new System.Drawing.Size(960, 50);
            this.radioPanel.TabIndex = 0;
            // 
            // buyCheck
            // 
            this.buyCheck.AutoSize = true;
            this.buyCheck.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buyCheck.Location = new System.Drawing.Point(243, 12);
            this.buyCheck.Name = "buyCheck";
            this.buyCheck.Size = new System.Drawing.Size(103, 20);
            this.buyCheck.TabIndex = 1;
            this.buyCheck.TabStop = true;
            this.buyCheck.Text = "Đặt lệnh mua";
            this.buyCheck.UseVisualStyleBackColor = true;
            this.buyCheck.CheckedChanged += new System.EventHandler(this.buyCheck_CheckedChanged);
            // 
            // sellCheck
            // 
            this.sellCheck.AutoSize = true;
            this.sellCheck.FlatAppearance.BorderColor = System.Drawing.SystemColors.MenuHighlight;
            this.sellCheck.FlatAppearance.BorderSize = 2;
            this.sellCheck.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sellCheck.Location = new System.Drawing.Point(63, 12);
            this.sellCheck.Name = "sellCheck";
            this.sellCheck.Size = new System.Drawing.Size(100, 20);
            this.sellCheck.TabIndex = 0;
            this.sellCheck.TabStop = true;
            this.sellCheck.Text = "Đặt lệnh bán";
            this.sellCheck.UseVisualStyleBackColor = true;
            this.sellCheck.CheckedChanged += new System.EventHandler(this.sellCheck_CheckedChanged);
            // 
            // adminPanel
            // 
            this.adminPanel.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.adminPanel.Controls.Add(this.btnAddNH);
            this.adminPanel.Controls.Add(this.btnAddCP);
            this.adminPanel.Controls.Add(this.btnBackupAndRestore);
            this.adminPanel.Controls.Add(this.btnAddLogin);
            this.adminPanel.Controls.Add(this.btnLogoutAdmin);
            this.adminPanel.Controls.Add(this.txtAdminName);
            this.adminPanel.Controls.Add(this.panel11);
            this.adminPanel.Controls.Add(this.txtAdminID);
            this.adminPanel.Controls.Add(this.panel12);
            this.adminPanel.Location = new System.Drawing.Point(0, 0);
            this.adminPanel.Name = "adminPanel";
            this.adminPanel.Size = new System.Drawing.Size(240, 600);
            this.adminPanel.TabIndex = 100;
            this.adminPanel.Visible = false;
            // 
            // btnAddNH
            // 
            this.btnAddNH.BackColor = System.Drawing.Color.Transparent;
            this.btnAddNH.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnAddNH.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DarkGreen;
            this.btnAddNH.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkGreen;
            this.btnAddNH.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddNH.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddNH.ForeColor = System.Drawing.Color.White;
            this.btnAddNH.Image = ((System.Drawing.Image)(resources.GetObject("btnAddNH.Image")));
            this.btnAddNH.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddNH.Location = new System.Drawing.Point(-2, 327);
            this.btnAddNH.Name = "btnAddNH";
            this.btnAddNH.Size = new System.Drawing.Size(246, 36);
            this.btnAddNH.TabIndex = 8;
            this.btnAddNH.Text = "Thêm ngân hàng";
            this.btnAddNH.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAddNH.UseVisualStyleBackColor = false;
            // 
            // btnAddCP
            // 
            this.btnAddCP.BackColor = System.Drawing.Color.Transparent;
            this.btnAddCP.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnAddCP.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DarkGreen;
            this.btnAddCP.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkGreen;
            this.btnAddCP.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddCP.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddCP.ForeColor = System.Drawing.Color.White;
            this.btnAddCP.Image = ((System.Drawing.Image)(resources.GetObject("btnAddCP.Image")));
            this.btnAddCP.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddCP.Location = new System.Drawing.Point(-3, 284);
            this.btnAddCP.Name = "btnAddCP";
            this.btnAddCP.Size = new System.Drawing.Size(246, 36);
            this.btnAddCP.TabIndex = 7;
            this.btnAddCP.Text = "Thêm cổ phiếu";
            this.btnAddCP.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAddCP.UseVisualStyleBackColor = false;
            // 
            // btnBackupAndRestore
            // 
            this.btnBackupAndRestore.BackColor = System.Drawing.Color.Transparent;
            this.btnBackupAndRestore.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnBackupAndRestore.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DarkGreen;
            this.btnBackupAndRestore.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkGreen;
            this.btnBackupAndRestore.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBackupAndRestore.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBackupAndRestore.ForeColor = System.Drawing.Color.White;
            this.btnBackupAndRestore.Image = ((System.Drawing.Image)(resources.GetObject("btnBackupAndRestore.Image")));
            this.btnBackupAndRestore.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBackupAndRestore.Location = new System.Drawing.Point(-3, 242);
            this.btnBackupAndRestore.Name = "btnBackupAndRestore";
            this.btnBackupAndRestore.Size = new System.Drawing.Size(246, 36);
            this.btnBackupAndRestore.TabIndex = 6;
            this.btnBackupAndRestore.Text = "Backup and Restore";
            this.btnBackupAndRestore.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBackupAndRestore.UseVisualStyleBackColor = false;
            this.btnBackupAndRestore.Click += new System.EventHandler(this.btnBackupAndRestore_Click);
            // 
            // btnAddLogin
            // 
            this.btnAddLogin.BackColor = System.Drawing.Color.Transparent;
            this.btnAddLogin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnAddLogin.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DarkGreen;
            this.btnAddLogin.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkGreen;
            this.btnAddLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddLogin.ForeColor = System.Drawing.Color.White;
            this.btnAddLogin.Image = ((System.Drawing.Image)(resources.GetObject("btnAddLogin.Image")));
            this.btnAddLogin.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddLogin.Location = new System.Drawing.Point(-2, 200);
            this.btnAddLogin.Name = "btnAddLogin";
            this.btnAddLogin.Size = new System.Drawing.Size(246, 36);
            this.btnAddLogin.TabIndex = 5;
            this.btnAddLogin.Text = "Tạo Login";
            this.btnAddLogin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAddLogin.UseVisualStyleBackColor = false;
            this.btnAddLogin.Click += new System.EventHandler(this.btnAddLogin_Click);
            // 
            // btnLogoutAdmin
            // 
            this.btnLogoutAdmin.BackColor = System.Drawing.Color.Transparent;
            this.btnLogoutAdmin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnLogoutAdmin.FlatAppearance.BorderSize = 0;
            this.btnLogoutAdmin.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DarkRed;
            this.btnLogoutAdmin.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkRed;
            this.btnLogoutAdmin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogoutAdmin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogoutAdmin.ForeColor = System.Drawing.Color.White;
            this.btnLogoutAdmin.Image = ((System.Drawing.Image)(resources.GetObject("btnLogoutAdmin.Image")));
            this.btnLogoutAdmin.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLogoutAdmin.Location = new System.Drawing.Point(12, 519);
            this.btnLogoutAdmin.Name = "btnLogoutAdmin";
            this.btnLogoutAdmin.Size = new System.Drawing.Size(120, 36);
            this.btnLogoutAdmin.TabIndex = 4;
            this.btnLogoutAdmin.Text = "Đăng xuất";
            this.btnLogoutAdmin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLogoutAdmin.UseVisualStyleBackColor = false;
            this.btnLogoutAdmin.Click += new System.EventHandler(this.btnLogoutAdmin_Click);
            // 
            // txtAdminName
            // 
            this.txtAdminName.AutoSize = true;
            this.txtAdminName.BackColor = System.Drawing.Color.Transparent;
            this.txtAdminName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAdminName.ForeColor = System.Drawing.Color.White;
            this.txtAdminName.Location = new System.Drawing.Point(5, 490);
            this.txtAdminName.Name = "txtAdminName";
            this.txtAdminName.Size = new System.Drawing.Size(104, 20);
            this.txtAdminName.TabIndex = 3;
            this.txtAdminName.Text = "NDT0000001";
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.White;
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel11.Location = new System.Drawing.Point(0, 450);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(240, 2);
            this.panel11.TabIndex = 2;
            // 
            // txtAdminID
            // 
            this.txtAdminID.AutoSize = true;
            this.txtAdminID.BackColor = System.Drawing.Color.Transparent;
            this.txtAdminID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAdminID.ForeColor = System.Drawing.Color.White;
            this.txtAdminID.Location = new System.Drawing.Point(5, 460);
            this.txtAdminID.Name = "txtAdminID";
            this.txtAdminID.Size = new System.Drawing.Size(104, 20);
            this.txtAdminID.TabIndex = 1;
            this.txtAdminID.Text = "NDT0000001";
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.Transparent;
            this.panel12.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel12.BackgroundImage")));
            this.panel12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panel12.Location = new System.Drawing.Point(70, 50);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(100, 100);
            this.panel12.TabIndex = 0;
            // 
            // saokePanel
            // 
            this.saokePanel.Controls.Add(this.btnHuyLenh);
            this.saokePanel.Controls.Add(this.listLenhCho);
            this.saokePanel.Controls.Add(this.label42);
            this.saokePanel.Controls.Add(this.panel20);
            this.saokePanel.Controls.Add(this.panel4);
            this.saokePanel.Location = new System.Drawing.Point(240, 0);
            this.saokePanel.Name = "saokePanel";
            this.saokePanel.Size = new System.Drawing.Size(1025, 565);
            this.saokePanel.TabIndex = 13;
            this.saokePanel.Visible = false;
            // 
            // btnHuyLenh
            // 
            this.btnHuyLenh.BackColor = System.Drawing.Color.DarkRed;
            this.btnHuyLenh.Enabled = false;
            this.btnHuyLenh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHuyLenh.ForeColor = System.Drawing.Color.White;
            this.btnHuyLenh.Image = ((System.Drawing.Image)(resources.GetObject("btnHuyLenh.Image")));
            this.btnHuyLenh.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnHuyLenh.Location = new System.Drawing.Point(866, 519);
            this.btnHuyLenh.Name = "btnHuyLenh";
            this.btnHuyLenh.Size = new System.Drawing.Size(124, 36);
            this.btnHuyLenh.TabIndex = 6;
            this.btnHuyLenh.Text = "Hủy lệnh";
            this.btnHuyLenh.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHuyLenh.UseVisualStyleBackColor = false;
            this.btnHuyLenh.Click += new System.EventHandler(this.btnHuyLenh_Click);
            // 
            // listLenhCho
            // 
            this.listLenhCho.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader11,
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8,
            this.columnHeader9,
            this.columnHeader10});
            this.listLenhCho.FullRowSelect = true;
            this.listLenhCho.Location = new System.Drawing.Point(142, 356);
            this.listLenhCho.MultiSelect = false;
            this.listLenhCho.Name = "listLenhCho";
            this.listLenhCho.Size = new System.Drawing.Size(705, 199);
            this.listLenhCho.TabIndex = 5;
            this.listLenhCho.UseCompatibleStateImageBehavior = false;
            this.listLenhCho.View = System.Windows.Forms.View.Details;
            this.listLenhCho.SelectedIndexChanged += new System.EventHandler(this.listLenhCho_SelectedIndexChanged);
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Mã cổ phiếu";
            this.columnHeader6.Width = 108;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Loại lệnh";
            this.columnHeader7.Width = 131;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Số lượng";
            this.columnHeader8.Width = 137;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Giá";
            this.columnHeader9.Width = 112;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Trạng thái";
            this.columnHeader10.Width = 120;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.White;
            this.label42.Location = new System.Drawing.Point(30, 330);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(119, 16);
            this.label42.TabIndex = 4;
            this.label42.Text = "Các lệnh đang chờ";
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.saokeDataContainer);
            this.panel20.Location = new System.Drawing.Point(30, 75);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(960, 243);
            this.panel20.TabIndex = 3;
            // 
            // saokeDataContainer
            // 
            this.saokeDataContainer.AllowUserToAddRows = false;
            this.saokeDataContainer.AllowUserToDeleteRows = false;
            this.saokeDataContainer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.saokeDataContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.saokeDataContainer.Location = new System.Drawing.Point(0, 0);
            this.saokeDataContainer.Name = "saokeDataContainer";
            this.saokeDataContainer.ReadOnly = true;
            this.saokeDataContainer.Size = new System.Drawing.Size(960, 243);
            this.saokeDataContainer.TabIndex = 0;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.btnSaoKeKhopInsize);
            this.panel4.Controls.Add(this.btnSaoKeInsize);
            this.panel4.Controls.Add(this.dateTo);
            this.panel4.Controls.Add(this.label46);
            this.panel4.Controls.Add(this.dateFrom);
            this.panel4.Controls.Add(this.label45);
            this.panel4.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.panel4.Location = new System.Drawing.Point(30, 15);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(960, 50);
            this.panel4.TabIndex = 2;
            // 
            // btnSaoKeKhopInsize
            // 
            this.btnSaoKeKhopInsize.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnSaoKeKhopInsize.FlatAppearance.BorderSize = 0;
            this.btnSaoKeKhopInsize.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DarkGreen;
            this.btnSaoKeKhopInsize.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkGreen;
            this.btnSaoKeKhopInsize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSaoKeKhopInsize.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaoKeKhopInsize.ForeColor = System.Drawing.Color.White;
            this.btnSaoKeKhopInsize.Image = ((System.Drawing.Image)(resources.GetObject("btnSaoKeKhopInsize.Image")));
            this.btnSaoKeKhopInsize.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSaoKeKhopInsize.Location = new System.Drawing.Point(780, 5);
            this.btnSaoKeKhopInsize.Name = "btnSaoKeKhopInsize";
            this.btnSaoKeKhopInsize.Size = new System.Drawing.Size(150, 36);
            this.btnSaoKeKhopInsize.TabIndex = 5;
            this.btnSaoKeKhopInsize.Text = "Sao kê khớp";
            this.btnSaoKeKhopInsize.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSaoKeKhopInsize.UseVisualStyleBackColor = false;
            this.btnSaoKeKhopInsize.Click += new System.EventHandler(this.btnSaoKeKhopInsize_Click);
            // 
            // btnSaoKeInsize
            // 
            this.btnSaoKeInsize.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnSaoKeInsize.FlatAppearance.BorderSize = 0;
            this.btnSaoKeInsize.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DarkGreen;
            this.btnSaoKeInsize.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkGreen;
            this.btnSaoKeInsize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSaoKeInsize.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaoKeInsize.ForeColor = System.Drawing.Color.White;
            this.btnSaoKeInsize.Image = ((System.Drawing.Image)(resources.GetObject("btnSaoKeInsize.Image")));
            this.btnSaoKeInsize.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSaoKeInsize.Location = new System.Drawing.Point(600, 5);
            this.btnSaoKeInsize.Name = "btnSaoKeInsize";
            this.btnSaoKeInsize.Size = new System.Drawing.Size(150, 36);
            this.btnSaoKeInsize.TabIndex = 4;
            this.btnSaoKeInsize.Text = "Sao kê";
            this.btnSaoKeInsize.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSaoKeInsize.UseVisualStyleBackColor = false;
            this.btnSaoKeInsize.Click += new System.EventHandler(this.btnSaoKeInsize_Click);
            // 
            // dateTo
            // 
            this.dateTo.Location = new System.Drawing.Point(389, 15);
            this.dateTo.Name = "dateTo";
            this.dateTo.Size = new System.Drawing.Size(193, 20);
            this.dateTo.TabIndex = 3;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(314, 16);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(68, 16);
            this.label46.TabIndex = 2;
            this.label46.Text = "Đến ngày:";
            // 
            // dateFrom
            // 
            this.dateFrom.Location = new System.Drawing.Point(90, 15);
            this.dateFrom.Name = "dateFrom";
            this.dateFrom.Size = new System.Drawing.Size(193, 20);
            this.dateFrom.TabIndex = 1;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(25, 15);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(60, 16);
            this.label45.TabIndex = 0;
            this.label45.Text = "Từ ngày:";
            // 
            // stockUpdatePanel
            // 
            this.stockUpdatePanel.Controls.Add(this.listCP);
            this.stockUpdatePanel.Controls.Add(this.sanGDGrid);
            this.stockUpdatePanel.Location = new System.Drawing.Point(240, 0);
            this.stockUpdatePanel.Name = "stockUpdatePanel";
            this.stockUpdatePanel.Size = new System.Drawing.Size(1025, 565);
            this.stockUpdatePanel.TabIndex = 14;
            this.stockUpdatePanel.Visible = false;
            // 
            // listCP
            // 
            this.listCP.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listCP.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5});
            this.listCP.Cursor = System.Windows.Forms.Cursors.Default;
            this.listCP.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listCP.Location = new System.Drawing.Point(64, 71);
            this.listCP.Name = "listCP";
            this.listCP.Size = new System.Drawing.Size(872, 222);
            this.listCP.TabIndex = 1;
            this.listCP.UseCompatibleStateImageBehavior = false;
            this.listCP.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Mã cổ phiếu";
            this.columnHeader1.Width = 95;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Tên công ty";
            this.columnHeader2.Width = 229;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Địa chỉ";
            this.columnHeader3.Width = 334;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Giá";
            this.columnHeader4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader4.Width = 150;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Trạng thái";
            this.columnHeader5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // sanGDGrid
            // 
            this.sanGDGrid.AllowUserToAddRows = false;
            this.sanGDGrid.AllowUserToDeleteRows = false;
            this.sanGDGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.sanGDGrid.Location = new System.Drawing.Point(18, 406);
            this.sanGDGrid.Name = "sanGDGrid";
            this.sanGDGrid.ReadOnly = true;
            this.sanGDGrid.Size = new System.Drawing.Size(968, 143);
            this.sanGDGrid.TabIndex = 0;
            // 
            // userInfoPanel
            // 
            this.userInfoPanel.Controls.Add(this.infoPanelDataGrid);
            this.userInfoPanel.Controls.Add(this.label26);
            this.userInfoPanel.Controls.Add(this.panel13);
            this.userInfoPanel.Location = new System.Drawing.Point(240, 0);
            this.userInfoPanel.Name = "userInfoPanel";
            this.userInfoPanel.Size = new System.Drawing.Size(1025, 565);
            this.userInfoPanel.TabIndex = 15;
            this.userInfoPanel.Visible = false;
            // 
            // infoPanelDataGrid
            // 
            this.infoPanelDataGrid.AllowUserToAddRows = false;
            this.infoPanelDataGrid.AllowUserToDeleteRows = false;
            this.infoPanelDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.infoPanelDataGrid.Location = new System.Drawing.Point(40, 205);
            this.infoPanelDataGrid.Name = "infoPanelDataGrid";
            this.infoPanelDataGrid.ReadOnly = true;
            this.infoPanelDataGrid.Size = new System.Drawing.Size(960, 350);
            this.infoPanelDataGrid.TabIndex = 7;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.DarkGreen;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.White;
            this.label26.Location = new System.Drawing.Point(40, 180);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(103, 16);
            this.label26.TabIndex = 6;
            this.label26.Text = "Cổ phiếu sở hữu";
            // 
            // panel13
            // 
            this.panel13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel13.Controls.Add(this.panel14);
            this.panel13.Controls.Add(this.cbInfoPanelBankID);
            this.panel13.Controls.Add(this.label20);
            this.panel13.Controls.Add(this.button1);
            this.panel13.Controls.Add(this.txtInfoPanelUserExchagePass);
            this.panel13.Controls.Add(this.label19);
            this.panel13.Controls.Add(this.btnInfoPanelChangeUserPass);
            this.panel13.Controls.Add(this.txtInfoPanelUserPass);
            this.panel13.Controls.Add(this.label18);
            this.panel13.Controls.Add(this.txtInfoPanelUserName);
            this.panel13.Controls.Add(this.label17);
            this.panel13.Controls.Add(this.txtInfoPanelUserID);
            this.panel13.Controls.Add(this.label15);
            this.panel13.Location = new System.Drawing.Point(40, 15);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(960, 150);
            this.panel13.TabIndex = 0;
            // 
            // panel14
            // 
            this.panel14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel14.Controls.Add(this.btnUngTien);
            this.panel14.Controls.Add(this.txtInfoPanelBalance);
            this.panel14.Controls.Add(this.txtInfoPanelBankName);
            this.panel14.Controls.Add(this.label25);
            this.panel14.Controls.Add(this.label24);
            this.panel14.Controls.Add(this.txtInfoPanelBankIDInfo);
            this.panel14.Controls.Add(this.label22);
            this.panel14.Location = new System.Drawing.Point(580, -1);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(380, 152);
            this.panel14.TabIndex = 1;
            // 
            // txtInfoPanelBalance
            // 
            this.txtInfoPanelBalance.AutoSize = true;
            this.txtInfoPanelBalance.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.txtInfoPanelBalance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInfoPanelBalance.ForeColor = System.Drawing.Color.White;
            this.txtInfoPanelBalance.Location = new System.Drawing.Point(85, 107);
            this.txtInfoPanelBalance.Name = "txtInfoPanelBalance";
            this.txtInfoPanelBalance.Size = new System.Drawing.Size(50, 16);
            this.txtInfoPanelBalance.TabIndex = 5;
            this.txtInfoPanelBalance.Text = "######";
            // 
            // txtInfoPanelBankName
            // 
            this.txtInfoPanelBankName.AutoSize = true;
            this.txtInfoPanelBankName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInfoPanelBankName.Location = new System.Drawing.Point(112, 59);
            this.txtInfoPanelBankName.Name = "txtInfoPanelBankName";
            this.txtInfoPanelBankName.Size = new System.Drawing.Size(50, 16);
            this.txtInfoPanelBankName.TabIndex = 4;
            this.txtInfoPanelBankName.Text = "######";
            this.txtInfoPanelBankName.Click += new System.EventHandler(this.txtInfoPanelBankName_Click);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(20, 107);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(46, 16);
            this.label25.TabIndex = 3;
            this.label25.Text = "Số dư:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(20, 59);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(77, 16);
            this.label24.TabIndex = 2;
            this.label24.Text = "Ngân hàng:";
            // 
            // txtInfoPanelBankIDInfo
            // 
            this.txtInfoPanelBankIDInfo.AutoSize = true;
            this.txtInfoPanelBankIDInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInfoPanelBankIDInfo.Location = new System.Drawing.Point(150, 17);
            this.txtInfoPanelBankIDInfo.Name = "txtInfoPanelBankIDInfo";
            this.txtInfoPanelBankIDInfo.Size = new System.Drawing.Size(50, 16);
            this.txtInfoPanelBankIDInfo.TabIndex = 1;
            this.txtInfoPanelBankIDInfo.Text = "######";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(20, 17);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(123, 16);
            this.label22.TabIndex = 0;
            this.label22.Text = "Thông tin tài khoản:";
            // 
            // cbInfoPanelBankID
            // 
            this.cbInfoPanelBankID.FormattingEnabled = true;
            this.cbInfoPanelBankID.Location = new System.Drawing.Point(391, 48);
            this.cbInfoPanelBankID.Name = "cbInfoPanelBankID";
            this.cbInfoPanelBankID.Size = new System.Drawing.Size(158, 21);
            this.cbInfoPanelBankID.TabIndex = 11;
            this.cbInfoPanelBankID.SelectedIndexChanged += new System.EventHandler(this.cbInfoPanelBankID_SelectedIndexChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(298, 53);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(87, 16);
            this.label20.TabIndex = 10;
            this.label20.Text = "Mã tài khoản:";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button1.BackgroundImage")));
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(514, 92);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(35, 30);
            this.button1.TabIndex = 9;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtInfoPanelUserExchagePass
            // 
            this.txtInfoPanelUserExchagePass.AutoSize = true;
            this.txtInfoPanelUserExchagePass.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInfoPanelUserExchagePass.Location = new System.Drawing.Point(428, 106);
            this.txtInfoPanelUserExchagePass.Name = "txtInfoPanelUserExchagePass";
            this.txtInfoPanelUserExchagePass.Size = new System.Drawing.Size(71, 16);
            this.txtInfoPanelUserExchagePass.TabIndex = 8;
            this.txtInfoPanelUserExchagePass.Text = "#########";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(298, 106);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(123, 16);
            this.label19.TabIndex = 7;
            this.label19.Text = "Mật khẩu giao dịch:";
            // 
            // btnInfoPanelChangeUserPass
            // 
            this.btnInfoPanelChangeUserPass.BackColor = System.Drawing.Color.Transparent;
            this.btnInfoPanelChangeUserPass.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnInfoPanelChangeUserPass.BackgroundImage")));
            this.btnInfoPanelChangeUserPass.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnInfoPanelChangeUserPass.FlatAppearance.BorderSize = 0;
            this.btnInfoPanelChangeUserPass.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInfoPanelChangeUserPass.Location = new System.Drawing.Point(220, 92);
            this.btnInfoPanelChangeUserPass.Name = "btnInfoPanelChangeUserPass";
            this.btnInfoPanelChangeUserPass.Size = new System.Drawing.Size(35, 30);
            this.btnInfoPanelChangeUserPass.TabIndex = 6;
            this.btnInfoPanelChangeUserPass.UseVisualStyleBackColor = false;
            this.btnInfoPanelChangeUserPass.Click += new System.EventHandler(this.btnInfoPanelChangeUserPass_Click);
            // 
            // txtInfoPanelUserPass
            // 
            this.txtInfoPanelUserPass.AutoSize = true;
            this.txtInfoPanelUserPass.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInfoPanelUserPass.Location = new System.Drawing.Point(97, 106);
            this.txtInfoPanelUserPass.Name = "txtInfoPanelUserPass";
            this.txtInfoPanelUserPass.Size = new System.Drawing.Size(71, 16);
            this.txtInfoPanelUserPass.TabIndex = 5;
            this.txtInfoPanelUserPass.Text = "#########";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(17, 106);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(65, 16);
            this.label18.TabIndex = 4;
            this.label18.Text = "Mật khẩu:";
            // 
            // txtInfoPanelUserName
            // 
            this.txtInfoPanelUserName.AutoSize = true;
            this.txtInfoPanelUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInfoPanelUserName.Location = new System.Drawing.Point(120, 65);
            this.txtInfoPanelUserName.Name = "txtInfoPanelUserName";
            this.txtInfoPanelUserName.Size = new System.Drawing.Size(70, 16);
            this.txtInfoPanelUserName.TabIndex = 3;
            this.txtInfoPanelUserName.Text = "#### ### #";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(15, 65);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(99, 16);
            this.label17.TabIndex = 2;
            this.label17.Text = "Tên nhà đầu tư:";
            // 
            // txtInfoPanelUserID
            // 
            this.txtInfoPanelUserID.AutoSize = true;
            this.txtInfoPanelUserID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInfoPanelUserID.Location = new System.Drawing.Point(120, 22);
            this.txtInfoPanelUserID.Name = "txtInfoPanelUserID";
            this.txtInfoPanelUserID.Size = new System.Drawing.Size(57, 16);
            this.txtInfoPanelUserID.TabIndex = 1;
            this.txtInfoPanelUserID.Text = "#######";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(20, 22);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(94, 16);
            this.label15.TabIndex = 0;
            this.label15.Text = "Mã nhà đầu tư:";
            // 
            // addLoginPanel
            // 
            this.addLoginPanel.Controls.Add(this.btnDeleteLogin);
            this.addLoginPanel.Controls.Add(this.txtLoginInfoDelete);
            this.addLoginPanel.Controls.Add(this.label36);
            this.addLoginPanel.Controls.Add(this.comboBox1);
            this.addLoginPanel.Controls.Add(this.panel17);
            this.addLoginPanel.Controls.Add(this.btnCreateLogin);
            this.addLoginPanel.Controls.Add(this.dtUserBirth);
            this.addLoginPanel.Controls.Add(this.label35);
            this.addLoginPanel.Controls.Add(this.tbCreateUserEmail);
            this.addLoginPanel.Controls.Add(this.tbCreateCMND);
            this.addLoginPanel.Controls.Add(this.tbCreateUserPhone);
            this.addLoginPanel.Controls.Add(this.tbCreateUserAddr);
            this.addLoginPanel.Controls.Add(this.tbCreateUserPass);
            this.addLoginPanel.Controls.Add(this.tbCreateUserName);
            this.addLoginPanel.Controls.Add(this.txtCreateUserID);
            this.addLoginPanel.Controls.Add(this.panel16);
            this.addLoginPanel.Controls.Add(this.label34);
            this.addLoginPanel.Controls.Add(this.label33);
            this.addLoginPanel.Controls.Add(this.label32);
            this.addLoginPanel.Controls.Add(this.label31);
            this.addLoginPanel.Controls.Add(this.label30);
            this.addLoginPanel.Controls.Add(this.label29);
            this.addLoginPanel.Controls.Add(this.label28);
            this.addLoginPanel.Controls.Add(this.label27);
            this.addLoginPanel.Location = new System.Drawing.Point(240, 0);
            this.addLoginPanel.Name = "addLoginPanel";
            this.addLoginPanel.Size = new System.Drawing.Size(1025, 565);
            this.addLoginPanel.TabIndex = 16;
            this.addLoginPanel.Visible = false;
            // 
            // btnDeleteLogin
            // 
            this.btnDeleteLogin.BackColor = System.Drawing.Color.DarkRed;
            this.btnDeleteLogin.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnDeleteLogin.FlatAppearance.BorderSize = 0;
            this.btnDeleteLogin.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DarkGreen;
            this.btnDeleteLogin.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkGreen;
            this.btnDeleteLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeleteLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteLogin.ForeColor = System.Drawing.Color.White;
            this.btnDeleteLogin.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteLogin.Image")));
            this.btnDeleteLogin.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDeleteLogin.Location = new System.Drawing.Point(736, 328);
            this.btnDeleteLogin.Name = "btnDeleteLogin";
            this.btnDeleteLogin.Size = new System.Drawing.Size(200, 36);
            this.btnDeleteLogin.TabIndex = 23;
            this.btnDeleteLogin.Text = "Xóa Login";
            this.btnDeleteLogin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDeleteLogin.UseVisualStyleBackColor = false;
            // 
            // txtLoginInfoDelete
            // 
            this.txtLoginInfoDelete.AutoSize = true;
            this.txtLoginInfoDelete.Location = new System.Drawing.Point(682, 182);
            this.txtLoginInfoDelete.Name = "txtLoginInfoDelete";
            this.txtLoginInfoDelete.Size = new System.Drawing.Size(41, 13);
            this.txtLoginInfoDelete.TabIndex = 22;
            this.txtLoginInfoDelete.Text = "label37";
            this.txtLoginInfoDelete.Visible = false;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(682, 143);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(44, 16);
            this.label36.TabIndex = 21;
            this.label36.Text = "Login:";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(768, 138);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(168, 21);
            this.comboBox1.TabIndex = 20;
            // 
            // panel17
            // 
            this.panel17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel17.Location = new System.Drawing.Point(565, -1);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(2, 565);
            this.panel17.TabIndex = 19;
            // 
            // btnCreateLogin
            // 
            this.btnCreateLogin.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnCreateLogin.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnCreateLogin.FlatAppearance.BorderSize = 0;
            this.btnCreateLogin.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DarkGreen;
            this.btnCreateLogin.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkGreen;
            this.btnCreateLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCreateLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreateLogin.ForeColor = System.Drawing.Color.White;
            this.btnCreateLogin.Image = ((System.Drawing.Image)(resources.GetObject("btnCreateLogin.Image")));
            this.btnCreateLogin.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCreateLogin.Location = new System.Drawing.Point(186, 482);
            this.btnCreateLogin.Name = "btnCreateLogin";
            this.btnCreateLogin.Size = new System.Drawing.Size(200, 36);
            this.btnCreateLogin.TabIndex = 18;
            this.btnCreateLogin.Text = "Tạo Login";
            this.btnCreateLogin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCreateLogin.UseVisualStyleBackColor = false;
            this.btnCreateLogin.Click += new System.EventHandler(this.btnCreateLogin_Click);
            // 
            // dtUserBirth
            // 
            this.dtUserBirth.Location = new System.Drawing.Point(186, 183);
            this.dtUserBirth.MinDate = new System.DateTime(1998, 12, 31, 0, 0, 0, 0);
            this.dtUserBirth.Name = "dtUserBirth";
            this.dtUserBirth.Size = new System.Drawing.Size(205, 20);
            this.dtUserBirth.TabIndex = 17;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(85, 182);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(71, 16);
            this.label35.TabIndex = 16;
            this.label35.Text = "Ngày sinh:";
            // 
            // tbCreateUserEmail
            // 
            this.tbCreateUserEmail.Location = new System.Drawing.Point(186, 399);
            this.tbCreateUserEmail.Name = "tbCreateUserEmail";
            this.tbCreateUserEmail.Size = new System.Drawing.Size(205, 20);
            this.tbCreateUserEmail.TabIndex = 15;
            // 
            // tbCreateCMND
            // 
            this.tbCreateCMND.Location = new System.Drawing.Point(186, 362);
            this.tbCreateCMND.Name = "tbCreateCMND";
            this.tbCreateCMND.Size = new System.Drawing.Size(205, 20);
            this.tbCreateCMND.TabIndex = 14;
            // 
            // tbCreateUserPhone
            // 
            this.tbCreateUserPhone.Location = new System.Drawing.Point(186, 313);
            this.tbCreateUserPhone.Name = "tbCreateUserPhone";
            this.tbCreateUserPhone.Size = new System.Drawing.Size(205, 20);
            this.tbCreateUserPhone.TabIndex = 13;
            // 
            // tbCreateUserAddr
            // 
            this.tbCreateUserAddr.Location = new System.Drawing.Point(186, 272);
            this.tbCreateUserAddr.Name = "tbCreateUserAddr";
            this.tbCreateUserAddr.Size = new System.Drawing.Size(205, 20);
            this.tbCreateUserAddr.TabIndex = 12;
            // 
            // tbCreateUserPass
            // 
            this.tbCreateUserPass.Location = new System.Drawing.Point(186, 223);
            this.tbCreateUserPass.Name = "tbCreateUserPass";
            this.tbCreateUserPass.Size = new System.Drawing.Size(205, 20);
            this.tbCreateUserPass.TabIndex = 11;
            // 
            // tbCreateUserName
            // 
            this.tbCreateUserName.Location = new System.Drawing.Point(186, 139);
            this.tbCreateUserName.Name = "tbCreateUserName";
            this.tbCreateUserName.Size = new System.Drawing.Size(205, 20);
            this.tbCreateUserName.TabIndex = 10;
            // 
            // txtCreateUserID
            // 
            this.txtCreateUserID.AutoSize = true;
            this.txtCreateUserID.Location = new System.Drawing.Point(183, 103);
            this.txtCreateUserID.Name = "txtCreateUserID";
            this.txtCreateUserID.Size = new System.Drawing.Size(56, 13);
            this.txtCreateUserID.TabIndex = 9;
            this.txtCreateUserID.Text = "#######";
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.ndtCheck);
            this.panel16.Controls.Add(this.adminCheck);
            this.panel16.Location = new System.Drawing.Point(186, 58);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(301, 31);
            this.panel16.TabIndex = 8;
            // 
            // ndtCheck
            // 
            this.ndtCheck.AutoSize = true;
            this.ndtCheck.Location = new System.Drawing.Point(191, 8);
            this.ndtCheck.Name = "ndtCheck";
            this.ndtCheck.Size = new System.Drawing.Size(79, 17);
            this.ndtCheck.TabIndex = 1;
            this.ndtCheck.TabStop = true;
            this.ndtCheck.Text = "Nhà đầu tư";
            this.ndtCheck.UseVisualStyleBackColor = true;
            this.ndtCheck.CheckedChanged += new System.EventHandler(this.ndtCheck_CheckedChanged);
            // 
            // adminCheck
            // 
            this.adminCheck.AutoSize = true;
            this.adminCheck.Location = new System.Drawing.Point(16, 9);
            this.adminCheck.Name = "adminCheck";
            this.adminCheck.Size = new System.Drawing.Size(54, 17);
            this.adminCheck.TabIndex = 0;
            this.adminCheck.TabStop = true;
            this.adminCheck.Text = "Admin";
            this.adminCheck.UseVisualStyleBackColor = true;
            this.adminCheck.CheckedChanged += new System.EventHandler(this.adminCheck_CheckedChanged);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(85, 402);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(45, 16);
            this.label34.TabIndex = 7;
            this.label34.Text = "Email:";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(85, 362);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(51, 16);
            this.label33.TabIndex = 6;
            this.label33.Text = "CMND:";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(85, 313);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(89, 16);
            this.label32.TabIndex = 5;
            this.label32.Text = "Số điện thoại:";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(85, 272);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(51, 16);
            this.label31.TabIndex = 4;
            this.label31.Text = "Địa chỉ:";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(85, 68);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(87, 16);
            this.label30.TabIndex = 3;
            this.label30.Text = "Nhóm quyền:";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(85, 224);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(65, 16);
            this.label29.TabIndex = 2;
            this.label29.Text = "Mật khẩu:";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(85, 100);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(77, 16);
            this.label28.TabIndex = 1;
            this.label28.Text = "User name:";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(85, 141);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(68, 16);
            this.label27.TabIndex = 0;
            this.label27.Text = "Họ và tên:";
            // 
            // addCPPanel
            // 
            this.addCPPanel.Controls.Add(this.button2);
            this.addCPPanel.Controls.Add(this.cbCreateOwnUser);
            this.addCPPanel.Controls.Add(this.tbCreateCPPrize);
            this.addCPPanel.Controls.Add(this.tbCreateCPAmount);
            this.addCPPanel.Controls.Add(this.tbCreateCPAddr);
            this.addCPPanel.Controls.Add(this.tbCreateCPComName);
            this.addCPPanel.Controls.Add(this.label41);
            this.addCPPanel.Controls.Add(this.label40);
            this.addCPPanel.Controls.Add(this.label39);
            this.addCPPanel.Controls.Add(this.label38);
            this.addCPPanel.Controls.Add(this.label37);
            this.addCPPanel.Location = new System.Drawing.Point(240, 0);
            this.addCPPanel.Name = "addCPPanel";
            this.addCPPanel.Size = new System.Drawing.Size(1025, 565);
            this.addCPPanel.TabIndex = 24;
            this.addCPPanel.Visible = false;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(180, 324);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(145, 26);
            this.button2.TabIndex = 10;
            this.button2.Text = "Thêm cổ phiếu";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // cbCreateOwnUser
            // 
            this.cbCreateOwnUser.FormattingEnabled = true;
            this.cbCreateOwnUser.Location = new System.Drawing.Point(202, 201);
            this.cbCreateOwnUser.Name = "cbCreateOwnUser";
            this.cbCreateOwnUser.Size = new System.Drawing.Size(176, 21);
            this.cbCreateOwnUser.TabIndex = 9;
            // 
            // tbCreateCPPrize
            // 
            this.tbCreateCPPrize.Location = new System.Drawing.Point(223, 253);
            this.tbCreateCPPrize.Name = "tbCreateCPPrize";
            this.tbCreateCPPrize.Size = new System.Drawing.Size(174, 20);
            this.tbCreateCPPrize.TabIndex = 8;
            // 
            // tbCreateCPAmount
            // 
            this.tbCreateCPAmount.Location = new System.Drawing.Point(202, 151);
            this.tbCreateCPAmount.Name = "tbCreateCPAmount";
            this.tbCreateCPAmount.Size = new System.Drawing.Size(174, 20);
            this.tbCreateCPAmount.TabIndex = 7;
            // 
            // tbCreateCPAddr
            // 
            this.tbCreateCPAddr.Location = new System.Drawing.Point(202, 105);
            this.tbCreateCPAddr.Name = "tbCreateCPAddr";
            this.tbCreateCPAddr.Size = new System.Drawing.Size(172, 20);
            this.tbCreateCPAddr.TabIndex = 6;
            // 
            // tbCreateCPComName
            // 
            this.tbCreateCPComName.Location = new System.Drawing.Point(202, 58);
            this.tbCreateCPComName.Name = "tbCreateCPComName";
            this.tbCreateCPComName.Size = new System.Drawing.Size(172, 20);
            this.tbCreateCPComName.TabIndex = 5;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(92, 255);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(120, 13);
            this.label41.TabIndex = 4;
            this.label41.Text = "Giá tham chiếu đầu tiên";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(92, 206);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(73, 13);
            this.label40.TabIndex = 3;
            this.label40.Text = "Người sở hữu:";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(92, 154);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(103, 13);
            this.label39.TabIndex = 2;
            this.label39.Text = "Số lượng phát hành:";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(92, 108);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(43, 13);
            this.label38.TabIndex = 1;
            this.label38.Text = "Địa chỉ:";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(92, 63);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(67, 13);
            this.label37.TabIndex = 0;
            this.label37.Text = "Tên công ty:";
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "Mã giao dịch";
            this.columnHeader11.Width = 95;
            // 
            // btnUngTien
            // 
            this.btnUngTien.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnUngTien.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUngTien.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUngTien.ForeColor = System.Drawing.Color.White;
            this.btnUngTien.Location = new System.Drawing.Point(229, 97);
            this.btnUngTien.Name = "btnUngTien";
            this.btnUngTien.Size = new System.Drawing.Size(139, 32);
            this.btnUngTien.TabIndex = 6;
            this.btnUngTien.Text = "Ứng tiền";
            this.btnUngTien.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUngTien.UseVisualStyleBackColor = false;
            this.btnUngTien.Click += new System.EventHandler(this.btnUngTien_Click);
            // 
            // Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(1264, 561);
            this.Controls.Add(this.userInfoPanel);
            this.Controls.Add(this.saokePanel);
            this.Controls.Add(this.menuPanel);
            this.Controls.Add(this.loginPanel);
            this.Controls.Add(this.stockUpdatePanel);
            this.Controls.Add(this.adminPanel);
            this.Controls.Add(this.mainPanel);
            this.Controls.Add(this.exchangePanel);
            this.Controls.Add(this.addCPPanel);
            this.Controls.Add(this.addLoginPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form";
            this.Load += new System.EventHandler(this.Form_Load);
            this.menuPanel.ResumeLayout(false);
            this.menuPanel.PerformLayout();
            this.loginPanel.ResumeLayout(false);
            this.loginPanel.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.exchangePanel.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.bankInfo.ResumeLayout(false);
            this.bankInfo.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.sumMoneyPanel.ResumeLayout(false);
            this.sumMoneyPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stockAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stockPrize)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.stockInfo.ResumeLayout(false);
            this.stockInfo.PerformLayout();
            this.radioPanel.ResumeLayout(false);
            this.radioPanel.PerformLayout();
            this.adminPanel.ResumeLayout(false);
            this.adminPanel.PerformLayout();
            this.saokePanel.ResumeLayout(false);
            this.saokePanel.PerformLayout();
            this.panel20.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.saokeDataContainer)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.stockUpdatePanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.sanGDGrid)).EndInit();
            this.userInfoPanel.ResumeLayout(false);
            this.userInfoPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.infoPanelDataGrid)).EndInit();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            this.addLoginPanel.ResumeLayout(false);
            this.addLoginPanel.PerformLayout();
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            this.addCPPanel.ResumeLayout(false);
            this.addCPPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbCreateCPPrize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCreateCPAmount)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel menuPanel;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnThongke;
        private System.Windows.Forms.Button btnSaoke;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnExchange;
        private System.Windows.Forms.Button btnLogout;
        private System.Windows.Forms.Label txtUserName;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label txtUserID;
        private System.Windows.Forms.Panel loginPanel;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.TextBox password;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TextBox user;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label info;
        private System.Windows.Forms.Panel mainPanel;
        private System.Windows.Forms.Panel exchangePanel;
        private System.Windows.Forms.Panel radioPanel;
        private System.Windows.Forms.RadioButton buyCheck;
        private System.Windows.Forms.RadioButton sellCheck;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox stockID;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.TextBox exchangePassword;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox bankID;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown stockAmount;
        private System.Windows.Forms.NumericUpDown stockPrize;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnCommit;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.RadioButton mpCheck;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.RadioButton loCheck;
        private System.Windows.Forms.Label txtExchangePassWarning;
        private System.Windows.Forms.Panel bankInfo;
        private System.Windows.Forms.Label txtBalance;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label txtBankName;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label txtBankID;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label txtPrizeWarning;
        private System.Windows.Forms.Label txtAmountWarning;
        private System.Windows.Forms.Panel stockInfo;
        private System.Windows.Forms.Label txtGiaTC;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label txtGiaSan;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label txtGiaTran;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label txtOwnStockAmount;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label txtStockID;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnSetting;
        private System.Windows.Forms.Label txtAvailableBalance;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel adminPanel;
        private System.Windows.Forms.Button btnBackupAndRestore;
        private System.Windows.Forms.Button btnAddLogin;
        private System.Windows.Forms.Button btnLogoutAdmin;
        private System.Windows.Forms.Label txtAdminName;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label txtAdminID;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel saokePanel;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnSaoKeInsize;
        private System.Windows.Forms.DateTimePicker dateTo;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.DateTimePicker dateFrom;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Button btnSaoKeKhopInsize;
        private System.Windows.Forms.DataGridView saokeDataContainer;
        private System.Windows.Forms.Panel stockUpdatePanel;
        private System.Windows.Forms.Panel userInfoPanel;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Button btnInfoPanelChangeUserPass;
        private System.Windows.Forms.Label txtInfoPanelUserPass;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label txtInfoPanelUserName;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label txtInfoPanelUserID;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Label txtInfoPanelBalance;
        private System.Windows.Forms.Label txtInfoPanelBankName;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label txtInfoPanelBankIDInfo;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ComboBox cbInfoPanelBankID;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label txtInfoPanelUserExchagePass;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.DataGridView infoPanelDataGrid;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.DataGridView sanGDGrid;
        private System.Windows.Forms.Button btnAddNH;
        private System.Windows.Forms.Button btnAddCP;
        private System.Windows.Forms.Panel addLoginPanel;
        private System.Windows.Forms.Button btnDeleteLogin;
        private System.Windows.Forms.Label txtLoginInfoDelete;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Button btnCreateLogin;
        private System.Windows.Forms.DateTimePicker dtUserBirth;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox tbCreateUserEmail;
        private System.Windows.Forms.TextBox tbCreateCMND;
        private System.Windows.Forms.TextBox tbCreateUserPhone;
        private System.Windows.Forms.TextBox tbCreateUserAddr;
        private System.Windows.Forms.TextBox tbCreateUserPass;
        private System.Windows.Forms.TextBox tbCreateUserName;
        private System.Windows.Forms.Label txtCreateUserID;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.RadioButton ndtCheck;
        private System.Windows.Forms.RadioButton adminCheck;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Panel addCPPanel;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ComboBox cbCreateOwnUser;
        private System.Windows.Forms.NumericUpDown tbCreateCPPrize;
        private System.Windows.Forms.NumericUpDown tbCreateCPAmount;
        private System.Windows.Forms.TextBox tbCreateCPAddr;
        private System.Windows.Forms.TextBox tbCreateCPComName;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Panel sumMoneyPanel;
        private System.Windows.Forms.Label txtSumMoney;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.ListView listCP;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ListView listLenhCho;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Button btnHuyLenh;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.Button btnUngTien;
    }
}