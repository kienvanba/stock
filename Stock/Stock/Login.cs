﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Stock {
    public partial class Login :System.Windows.Forms.Form {
        private Form main;
        private String exchangePass;
        public Login(Form main, String exchangePass) {
            main.Enabled = false;
            InitializeComponent();
            this.main = main;
            this.exchangePass = exchangePass;
        }

        private void button2_Click(object sender, EventArgs e) {
            closeForm();
        }

        private void btnConfirm_Click(object sender, EventArgs e) {
            if(exchangePass == null) {
                if(!txtOld.Text.ToString().Equals(Program.passwd)) {
                    txtWarning.Visible = true;
                    return;
                }
                try {
                    String sql = "dbo.sp_password '" + txtOld.Text.ToString() + "' '" + txtNew.Text.ToString() + "' '" + Program.userId + "'";
                    Program.ExecSqlQuery(sql);
                    Program.passwd = txtNew.Text.ToString();
                } catch(Exception ex) {
                    txtWarning.Text = ex.ToString();
                    txtWarning.Visible = true;
                }
            }else {
                if(!txtOld.Text.ToString().Equals(exchangePass)) {
                    txtWarning.Visible = true;
                    return;
                }
                try {
                    String sql = "UPDATE NDT SET MKGD='" + txtNew.Text.ToString() + "' WHERE MANDT='" + Program.userId + "'";
                    Program.ExecSqlQuery(sql);
                }catch(Exception ex) {
                    txtWarning.Text = ex.ToString();
                    txtWarning.Visible = true;
                }
            }
        }

        private void txtOld_TextChanged(object sender, EventArgs e) {
            if(txtWarning.Visible)
                txtWarning.Visible = false;
        }

        private void closeForm() {
            main.Enabled = true;
            this.Close();
        }
    }
}
