﻿using Stock.code;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Stock {
    static class Program {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        public static SqlConnection conn = new SqlConnection();
        public static String connstr;
        public static SqlDataAdapter da;
        public static SqlDataReader myReader;
        public static String servername = "DESKTOP-ECGF433\\SERVER";
        public static String database = "CHUNGKHOAN";
        public static String userId;
        public static String userName;
        public static String userGroup;
        public static String passwd;

        public static int connect(String login, String password) {
            if(Program.conn != null && Program.conn.State == ConnectionState.Open)
                Program.conn.Close();
            try {
                Program.connstr = "Data Source=" + Program.servername + ";Initial Catalog=" + Program.database + ";User ID=" +
                      login + ";password=" + password+ ";MultipleActiveResultSets=True";
                Program.conn.ConnectionString = Program.connstr;
                Program.conn.Open();
                Program.passwd = password;
                return 1;
            } catch(Exception e) {
                return 0;
            }
        }
        public static SqlDataReader ExecSqlDataReader(String cmd) {
            SqlDataReader myreader;

            SqlCommand sqlcmd = new SqlCommand();
            sqlcmd.Connection = Program.conn;
            sqlcmd.CommandText = cmd;
            sqlcmd.CommandType = CommandType.Text;

            if(Program.conn.State == ConnectionState.Closed)
                Program.conn.Open();
            try {
                myreader = sqlcmd.ExecuteReader();
                return myreader;
            } catch(SqlException ex) {
                Program.conn.Close();
                MessageBox.Show(ex.Message);
                return null;
            }
        }
        public static DataTable ExecDataTable(String cmd, List<Parameter> parameters) {
            if(Program.conn.State == ConnectionState.Closed)
                Program.conn.Open();
            DataTable dt = new DataTable();
            SqlCommand command = new SqlCommand(cmd, Program.conn);
            command.CommandType = CommandType.StoredProcedure;
            foreach(Parameter p in parameters) {
                command.Parameters.Add(p.name, p.type).Value = p.value;
            }
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(dt);
            return dt;
        }
        public static object ExecIntOutPut(String cmd, List<Parameter> parameters, String outputName) {
            if(Program.conn != null && Program.conn.State == ConnectionState.Closed)
                Program.conn.Open();
            SqlCommand command = Program.conn.CreateCommand();
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = cmd;
            foreach(Parameter p in parameters) {
                command.Parameters.Add(p.name, p.type).Value = p.value;
            }
            command.Parameters.Add(outputName, SqlDbType.Int).Direction = ParameterDirection.Output;
            command.ExecuteNonQuery();
            Program.conn.Close();
            return command.Parameters[outputName].Value;
        }
        public static object ExecStringOutPut(String cmd, String outputName) {
            if(Program.conn != null && Program.conn.State == ConnectionState.Closed)
                Program.conn.Open();
            SqlCommand command = Program.conn.CreateCommand();
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = cmd;
            command.Parameters.Add(outputName, SqlDbType.NChar).Direction = ParameterDirection.Output;
            command.ExecuteNonQuery();
            Program.conn.Close();
            return command.Parameters[outputName].Value;
        }
        public static DataTable ExecSqlQuery(String cmd) {
            DataTable dt = new DataTable();
            conn = new SqlConnection(connstr);
            da = new SqlDataAdapter(cmd, conn);
            da.Fill(dt);
            return dt;
        }
        public static void logout() {
            if(Program.conn != null && Program.conn.State == ConnectionState.Open)
                Program.conn.Close();
            userId = null;
            userName = null;
            userGroup = null;
            passwd = null;
        }



        [STAThread]
        static void Main() {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form());
        }
    }
}
