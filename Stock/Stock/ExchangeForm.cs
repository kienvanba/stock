﻿using Stock.code;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace Stock {
    public partial class ExchangeForm :System.Windows.Forms.Form {
        private string exchangePass = "###";
        private int ownedStock = 0;
        private float currentStockMinPrize = 0.01f;
        private float currentStockMaxPrize = float.MaxValue;

        private List<Parameter> parameters;
        public ExchangeForm() {
            InitializeComponent();
        }

        private void ExchangeForm_Load(object sender, EventArgs e) {
            parameters = new List<Parameter>();
            sellCheck.Checked = true;
            stockPrize.Enabled = loCheck.Checked;
            loCheck.Checked = true;
            stockInfo.Visible = false;
            String sql = "exec SP_GET_TK_SOHUU '" + Program.userId + "'";
            DataTable dt = Program.ExecSqlQuery(sql);
            bankID.DataSource = dt;
            bankID.ValueMember = "MATK";
            bankID.DisplayMember = "TENNH";

            setStock(sellCheck.Checked);
            ownedStockInfo.Visible = sellCheck.Checked;
        }

        private void bankID_SelectedIndexChanged(object sender, EventArgs e) {
            //txtBalance.Text = bankID.SelectedValue.ToString();
            //String sql = "exec TT_NDT '" + Program.userId + "', '" + bankID.SelectedValue.ToString() + "'";
            DataTable dataTable = new DataTable();
            SqlCommand command = new SqlCommand("dbo.TT_NDT", Program.conn);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add("@MANDT", SqlDbType.NChar).Value = Program.userId;
            command.Parameters.Add("@MATK", SqlDbType.NChar).Value = bankID.SelectedValue.ToString();
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(dataTable);
            foreach(DataRow row in dataTable.Rows) {
                //txtBalance.Text = row["SOTIEN"].ToString();
                exchangePass = row["MKGD"].ToString();
            }
            //txtBalance.Text = dataTable.Rows[0]["SOTIEN"].ToString();
            //for(int i=0; i<dataTable.Rows.Count;i++) {
            //    txtBalance.Text = dataTable.Rows[i]["SOTIEN"].ToString();
            //    Debug.WriteLine(i);
            //}
        }

        private void sellCheck_CheckedChanged(object sender, EventArgs e) {
            setStock(sellCheck.Checked);
            stockInfo.Visible = false;
            ownedStockInfo.Visible = true;
        }

        private void setStock(bool sell) {
            String sql = "";
            parameters.Clear();
            if(sell) {
                parameters.Add(new Parameter("@MANDT", SqlDbType.NChar, Program.userId));
                sql = "dbo.SP_GET_ALL_SOHUU";
            }else {
                sql = "dbo.SP_GET_ALL_COPHIEU";
            }
            DataTable dataTable = Program.ExecDataTable(sql, parameters);
            stockID.DataSource = dataTable;
            stockID.ValueMember = "MACP";
            stockID.DisplayMember = "MACP";
            ownedStockGrid.DataSource = dataTable;
        }

        private void buyCheck_CheckedChanged(object sender, EventArgs e) {
            setStock(sellCheck.Checked);
            stockInfo.Visible = true;
            ownedStockInfo.Visible = false;
        }

        private void stockID_SelectedIndexChanged(object sender, EventArgs e) {
            loadStockInfo();
        }

        private void loadStockInfo() {
            if(buyCheck.Checked) {
                currentStockMinPrize = 0.01f;
                currentStockMaxPrize = float.MaxValue;
                txtGiaSan.Text = "na";
                txtGiaTran.Text = "na";
                txtGiaTC.Text = "na";
                stockInfo.Visible = true;
                String sql = "exec SP_GET_TT_CP '" + stockID.SelectedValue.ToString() + "'";
                DataTable dt = Program.ExecSqlQuery(sql);
                foreach(DataRow row in dt.Rows) {
                    txtGiaSan.Text = row["GIASAN"].ToString();
                    txtGiaTran.Text = row["GIATRAN"].ToString();
                    txtGiaTC.Text = row["GIATC"].ToString();
                    currentStockMinPrize = float.Parse(row["GIASAN"].ToString());
                    currentStockMaxPrize = float.Parse(row["GIATRAN"].ToString());
                }
            }
            if(sellCheck.Checked) {
                ownedStockInfo.Visible = true;
                parameters.Clear();
                String sql = "dbo.SP_GET_SOHUU";
                parameters.Add(new Parameter("@MANDT", SqlDbType.NChar, Program.userId));
                parameters.Add(new Parameter("@MACP", SqlDbType.NChar, stockID.SelectedValue.ToString()));
                try {
                    if(Program.ExecIntOutPut(sql, parameters, "@OWN") != DBNull.Value) {
                        ownedStock = Int32.Parse(Program.ExecIntOutPut(sql, parameters, "@OWN").ToString());
                    }
                } catch(Exception ex) {
                    MessageBox.Show(ex.ToString());
                }
                ownStockAmount.Text = ownedStock.ToString();
            }
        }

        private void stockAmount_ValueChanged(object sender, EventArgs e) {
            errorProvider1.SetError(stockAmount, null);
        }

        private void stockPrize_ValueChanged(object sender, EventArgs e) {
            errorProvider1.SetError(stockAmount, null);
        }

        private void exchangePassword_TextChanged(object sender, EventArgs e) {
            errorProvider1.SetError(exchangePassword, null);
        }

        private void loCheck_CheckedChanged(object sender, EventArgs e) {
            stockPrize.Enabled = true;
        }

        private void mpCheck_CheckedChanged(object sender, EventArgs e) {
            stockPrize.Enabled = false;
        }

        private void refreshForm() {
            setStock(sellCheck.Checked);
            ownedStock = 0;
            stockAmount.Value = 1;
            stockPrize.Value = 0;
            exchangePassword.Text = "";
            loadStockInfo();
        }

        private void btnCommit_Click(object sender, EventArgs e) {
            if(!exchangePassword.Text.Equals(exchangePass)) {
                errorProvider1.SetError(exchangePassword, "invalid password");
                return;
            }
            if((int) stockAmount.Value == 0) {
                errorProvider1.SetError(stockAmount, "invalid amount");
                return;
            }
            if((int) stockPrize.Value <= 0 && loCheck.Checked) {
                errorProvider1.SetError(stockPrize, "invalid prize");
                return;
            }
            if(buyCheck.Checked) {
                if(((float) stockPrize.Value < currentStockMinPrize || (float) stockPrize.Value >= currentStockMaxPrize) && loCheck.Checked) {
                    errorProvider1.SetError(stockPrize, "invalid prize");
                    return;
                }
                parameters.Clear();
                parameters.Add(new Parameter("@LOAILENH", SqlDbType.Char, "M"));
            }
            if(sellCheck.Checked) {
                if((int)stockAmount.Value<=0 || (int) stockAmount.Value > ownedStock) {
                    errorProvider1.SetError(stockAmount, "invalid amount");
                    return;
                }
                parameters.Clear();
                parameters.Add(new Parameter("@LOAILENH", SqlDbType.Char, "B"));
            }
            try {
                String pt = loCheck.Checked ? "LO" : "MP";
                parameters.Add(new Parameter("@PHUONGTHUC", SqlDbType.NChar, pt));
                parameters.Add(new Parameter("@SOLUONG", SqlDbType.Int, (int) stockAmount.Value));
                parameters.Add(new Parameter("@MACP", SqlDbType.NChar, stockID.SelectedValue.ToString()));
                parameters.Add(new Parameter("@GIA", SqlDbType.Float, Math.Round((float) stockPrize.Value, 2)));
                parameters.Add(new Parameter("@MATK", SqlDbType.NChar, bankID.SelectedValue.ToString()));
                parameters.Add(new Parameter("@MANDT", SqlDbType.NChar, Program.userId));
                String sql = "dbo.SP_EXCHANGE";
                Program.ExecDataTable(sql, parameters);
                MessageBox.Show(sellCheck.Checked?"Đặt bán":"Đặt mua"+" thành công!\nCổ phiếu: " + stockID.SelectedValue
                    + "\nSố lượng: " + stockAmount.Value.ToString()
                    + "\nGiá: " + Math.Round((float) stockPrize.Value, 2).ToString()
                    + "\nPhương thức giao dịch: " + pt);

                refreshForm();
            } catch(Exception ex) {
                Debug.WriteLine(ex.ToString());
            }
        }
    }
}
