﻿namespace Stock {
    partial class ExchangeForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.stockID = new System.Windows.Forms.ComboBox();
            this.loCheck = new System.Windows.Forms.RadioButton();
            this.mpCheck = new System.Windows.Forms.RadioButton();
            this.stockAmount = new System.Windows.Forms.NumericUpDown();
            this.stockPrize = new System.Windows.Forms.NumericUpDown();
            this.exchangePassword = new System.Windows.Forms.TextBox();
            this.bankID = new System.Windows.Forms.ComboBox();
            this.sellCheck = new System.Windows.Forms.RadioButton();
            this.buyCheck = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCommit = new System.Windows.Forms.Button();
            this.stockInfo = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.txtGiaTC = new System.Windows.Forms.Label();
            this.txtGiaTran = new System.Windows.Forms.Label();
            this.txtGiaSan = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.ownedStockInfo = new System.Windows.Forms.Panel();
            this.ownedStockGrid = new System.Windows.Forms.DataGridView();
            this.ownStockAmount = new System.Windows.Forms.Label();
            this.lable = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.stockAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stockPrize)).BeginInit();
            this.panel1.SuspendLayout();
            this.stockInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.ownedStockInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ownedStockGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // stockID
            // 
            this.stockID.FormattingEnabled = true;
            this.stockID.Location = new System.Drawing.Point(184, 148);
            this.stockID.Name = "stockID";
            this.stockID.Size = new System.Drawing.Size(121, 21);
            this.stockID.TabIndex = 3;
            this.stockID.SelectedIndexChanged += new System.EventHandler(this.stockID_SelectedIndexChanged);
            // 
            // loCheck
            // 
            this.loCheck.AutoSize = true;
            this.loCheck.Location = new System.Drawing.Point(184, 185);
            this.loCheck.Name = "loCheck";
            this.loCheck.Size = new System.Drawing.Size(39, 17);
            this.loCheck.TabIndex = 4;
            this.loCheck.TabStop = true;
            this.loCheck.Text = "LO";
            this.loCheck.UseVisualStyleBackColor = true;
            this.loCheck.CheckedChanged += new System.EventHandler(this.loCheck_CheckedChanged);
            // 
            // mpCheck
            // 
            this.mpCheck.AutoSize = true;
            this.mpCheck.Location = new System.Drawing.Point(264, 185);
            this.mpCheck.Name = "mpCheck";
            this.mpCheck.Size = new System.Drawing.Size(41, 17);
            this.mpCheck.TabIndex = 5;
            this.mpCheck.TabStop = true;
            this.mpCheck.Text = "MP";
            this.mpCheck.UseVisualStyleBackColor = true;
            this.mpCheck.CheckedChanged += new System.EventHandler(this.mpCheck_CheckedChanged);
            // 
            // stockAmount
            // 
            this.stockAmount.Location = new System.Drawing.Point(185, 223);
            this.stockAmount.Maximum = new decimal(new int[] {
            -727379968,
            232,
            0,
            0});
            this.stockAmount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.stockAmount.Name = "stockAmount";
            this.stockAmount.Size = new System.Drawing.Size(120, 20);
            this.stockAmount.TabIndex = 7;
            this.stockAmount.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.stockAmount.ValueChanged += new System.EventHandler(this.stockAmount_ValueChanged);
            // 
            // stockPrize
            // 
            this.stockPrize.DecimalPlaces = 2;
            this.stockPrize.Location = new System.Drawing.Point(185, 275);
            this.stockPrize.Maximum = new decimal(new int[] {
            1410065408,
            2,
            0,
            0});
            this.stockPrize.Name = "stockPrize";
            this.stockPrize.Size = new System.Drawing.Size(120, 20);
            this.stockPrize.TabIndex = 8;
            this.stockPrize.ValueChanged += new System.EventHandler(this.stockPrize_ValueChanged);
            // 
            // exchangePassword
            // 
            this.exchangePassword.Location = new System.Drawing.Point(185, 391);
            this.exchangePassword.Name = "exchangePassword";
            this.exchangePassword.Size = new System.Drawing.Size(164, 20);
            this.exchangePassword.TabIndex = 9;
            this.exchangePassword.UseSystemPasswordChar = true;
            this.exchangePassword.TextChanged += new System.EventHandler(this.exchangePassword_TextChanged);
            // 
            // bankID
            // 
            this.bankID.FormattingEnabled = true;
            this.bankID.Location = new System.Drawing.Point(184, 332);
            this.bankID.Name = "bankID";
            this.bankID.Size = new System.Drawing.Size(162, 21);
            this.bankID.TabIndex = 10;
            this.bankID.SelectedIndexChanged += new System.EventHandler(this.bankID_SelectedIndexChanged);
            // 
            // sellCheck
            // 
            this.sellCheck.AutoSize = true;
            this.sellCheck.Location = new System.Drawing.Point(3, 12);
            this.sellCheck.Name = "sellCheck";
            this.sellCheck.Size = new System.Drawing.Size(42, 17);
            this.sellCheck.TabIndex = 11;
            this.sellCheck.TabStop = true;
            this.sellCheck.Text = "Sell";
            this.sellCheck.UseVisualStyleBackColor = true;
            this.sellCheck.CheckedChanged += new System.EventHandler(this.sellCheck_CheckedChanged);
            // 
            // buyCheck
            // 
            this.buyCheck.AutoSize = true;
            this.buyCheck.Location = new System.Drawing.Point(104, 12);
            this.buyCheck.Name = "buyCheck";
            this.buyCheck.Size = new System.Drawing.Size(43, 17);
            this.buyCheck.TabIndex = 12;
            this.buyCheck.TabStop = true;
            this.buyCheck.Text = "Buy";
            this.buyCheck.UseVisualStyleBackColor = true;
            this.buyCheck.CheckedChanged += new System.EventHandler(this.buyCheck_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buyCheck);
            this.panel1.Controls.Add(this.sellCheck);
            this.panel1.Location = new System.Drawing.Point(184, 73);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(164, 37);
            this.panel1.TabIndex = 14;
            // 
            // btnCommit
            // 
            this.btnCommit.Location = new System.Drawing.Point(435, 419);
            this.btnCommit.Name = "btnCommit";
            this.btnCommit.Size = new System.Drawing.Size(75, 23);
            this.btnCommit.TabIndex = 15;
            this.btnCommit.Text = "Commit";
            this.btnCommit.UseVisualStyleBackColor = true;
            this.btnCommit.Click += new System.EventHandler(this.btnCommit_Click);
            // 
            // stockInfo
            // 
            this.stockInfo.Controls.Add(this.label1);
            this.stockInfo.Controls.Add(this.txtGiaTC);
            this.stockInfo.Controls.Add(this.txtGiaTran);
            this.stockInfo.Controls.Add(this.txtGiaSan);
            this.stockInfo.Controls.Add(this.label3);
            this.stockInfo.Controls.Add(this.label2);
            this.stockInfo.Location = new System.Drawing.Point(18, 297);
            this.stockInfo.Name = "stockInfo";
            this.stockInfo.Size = new System.Drawing.Size(161, 114);
            this.stockInfo.TabIndex = 16;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Gia Tran";
            // 
            // txtGiaTC
            // 
            this.txtGiaTC.AutoSize = true;
            this.txtGiaTC.Location = new System.Drawing.Point(74, 83);
            this.txtGiaTC.Name = "txtGiaTC";
            this.txtGiaTC.Size = new System.Drawing.Size(25, 13);
            this.txtGiaTC.TabIndex = 5;
            this.txtGiaTC.Text = "ccc";
            // 
            // txtGiaTran
            // 
            this.txtGiaTran.AutoSize = true;
            this.txtGiaTran.Location = new System.Drawing.Point(74, 29);
            this.txtGiaTran.Name = "txtGiaTran";
            this.txtGiaTran.Size = new System.Drawing.Size(25, 13);
            this.txtGiaTran.TabIndex = 1;
            this.txtGiaTran.Text = "aaa";
            // 
            // txtGiaSan
            // 
            this.txtGiaSan.AutoSize = true;
            this.txtGiaSan.Location = new System.Drawing.Point(74, 58);
            this.txtGiaSan.Name = "txtGiaSan";
            this.txtGiaSan.Size = new System.Drawing.Size(25, 13);
            this.txtGiaSan.TabIndex = 3;
            this.txtGiaSan.Text = "bbb";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Gia TC";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Gia San";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // ownedStockInfo
            // 
            this.ownedStockInfo.Controls.Add(this.ownedStockGrid);
            this.ownedStockInfo.Controls.Add(this.ownStockAmount);
            this.ownedStockInfo.Controls.Add(this.lable);
            this.ownedStockInfo.Location = new System.Drawing.Point(373, 111);
            this.ownedStockInfo.Name = "ownedStockInfo";
            this.ownedStockInfo.Size = new System.Drawing.Size(849, 216);
            this.ownedStockInfo.TabIndex = 7;
            // 
            // ownedStockGrid
            // 
            this.ownedStockGrid.AllowUserToAddRows = false;
            this.ownedStockGrid.AllowUserToDeleteRows = false;
            this.ownedStockGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ownedStockGrid.Location = new System.Drawing.Point(441, 3);
            this.ownedStockGrid.Name = "ownedStockGrid";
            this.ownedStockGrid.ReadOnly = true;
            this.ownedStockGrid.Size = new System.Drawing.Size(405, 210);
            this.ownedStockGrid.TabIndex = 2;
            // 
            // ownStockAmount
            // 
            this.ownStockAmount.AutoSize = true;
            this.ownStockAmount.Location = new System.Drawing.Point(67, 22);
            this.ownStockAmount.Name = "ownStockAmount";
            this.ownStockAmount.Size = new System.Drawing.Size(19, 13);
            this.ownStockAmount.TabIndex = 1;
            this.ownStockAmount.Text = "na";
            // 
            // lable
            // 
            this.lable.AutoSize = true;
            this.lable.Location = new System.Drawing.Point(9, 22);
            this.lable.Name = "lable";
            this.lable.Size = new System.Drawing.Size(43, 13);
            this.lable.TabIndex = 0;
            this.lable.Text = "Amount";
            // 
            // ExchangeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 561);
            this.Controls.Add(this.ownedStockInfo);
            this.Controls.Add(this.stockInfo);
            this.Controls.Add(this.btnCommit);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.bankID);
            this.Controls.Add(this.exchangePassword);
            this.Controls.Add(this.stockPrize);
            this.Controls.Add(this.stockAmount);
            this.Controls.Add(this.mpCheck);
            this.Controls.Add(this.loCheck);
            this.Controls.Add(this.stockID);
            this.Name = "ExchangeForm";
            this.Text = "ExchangeForm";
            this.Load += new System.EventHandler(this.ExchangeForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.stockAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stockPrize)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.stockInfo.ResumeLayout(false);
            this.stockInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ownedStockInfo.ResumeLayout(false);
            this.ownedStockInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ownedStockGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox stockID;
        private System.Windows.Forms.RadioButton loCheck;
        private System.Windows.Forms.RadioButton mpCheck;
        private System.Windows.Forms.NumericUpDown stockAmount;
        private System.Windows.Forms.NumericUpDown stockPrize;
        private System.Windows.Forms.TextBox exchangePassword;
        private System.Windows.Forms.ComboBox bankID;
        private System.Windows.Forms.RadioButton sellCheck;
        private System.Windows.Forms.RadioButton buyCheck;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnCommit;
        private System.Windows.Forms.Panel stockInfo;
        private System.Windows.Forms.Label txtGiaTC;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label txtGiaSan;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label txtGiaTran;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Panel ownedStockInfo;
        private System.Windows.Forms.Label ownStockAmount;
        private System.Windows.Forms.Label lable;
        private System.Windows.Forms.DataGridView ownedStockGrid;
    }
}