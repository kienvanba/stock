﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stock.code {
    class Parameter {
        public Parameter(String name, SqlDbType type, object value){
            this.name = name;
            this.type = type;
            this.value = value;
        }
        public String name { get; set;}
        public SqlDbType type { get; set;}
        public object value { get; set;}
    }
}
